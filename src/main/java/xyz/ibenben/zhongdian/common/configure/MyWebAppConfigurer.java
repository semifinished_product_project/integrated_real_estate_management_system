package xyz.ibenben.zhongdian.common.configure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import xyz.ibenben.zhongdian.common.converter.UniversalEnumConverterFactory;
import xyz.ibenben.zhongdian.common.intercepter.MyInterceptor;

/**
 * 配置Web
 *
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
@Configuration
public class MyWebAppConfigurer extends WebMvcConfigurerAdapter {
    /**
     * 增加枚举转换工厂
     *
     * @param registry 参数
     */
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(new UniversalEnumConverterFactory());
    }

    /**
     * 此方法把该拦截器实例化成一个bean,否则在拦截器里无法注入其它bean
     *
     * @return 拦截器
     */
    @Bean
    MyInterceptor myInterceptor() {
        return new MyInterceptor();
    }

    /**
     * 把拦截器注册到spring
     *
     * @param registry 参数
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 多个拦截器组成一个拦截器链
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
        registry.addInterceptor(myInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/error");
        super.addInterceptors(registry);
    }

}
