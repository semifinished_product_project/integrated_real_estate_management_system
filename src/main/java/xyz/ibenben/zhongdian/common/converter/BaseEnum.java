package xyz.ibenben.zhongdian.common.converter;

/**
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
public interface BaseEnum<T> {
    /**
     * 获取值
     *
     * @return 返回值
     */
    T getValue();

    /**
     * 获取文字
     *
     * @return 返回值
     */
    String getText();
}
