package xyz.ibenben.zhongdian.common.intercepter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import xyz.ibenben.zhongdian.system.entity.ChinaCity;
import xyz.ibenben.zhongdian.system.entity.ChinaProvince;
import xyz.ibenben.zhongdian.system.entity.ChinaRegion;
import xyz.ibenben.zhongdian.system.entity.HouseInfo;
import xyz.ibenben.zhongdian.system.entity.elasticsearch.ElasticsearchHouseInfo;
import xyz.ibenben.zhongdian.system.entity.mongodb.MongoChinaCity;
import xyz.ibenben.zhongdian.system.entity.mongodb.MongoChinaProvince;
import xyz.ibenben.zhongdian.system.entity.mongodb.MongoChinaRegion;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;
import xyz.ibenben.zhongdian.system.service.ChinaCityService;
import xyz.ibenben.zhongdian.system.service.ChinaProvinceService;
import xyz.ibenben.zhongdian.system.service.ChinaRegionService;
import xyz.ibenben.zhongdian.system.service.HouseInfoService;
import xyz.ibenben.zhongdian.system.service.elasticsearch.ElasticsearchHouseInfoService;
import xyz.ibenben.zhongdian.system.service.impl.RedisService;
import xyz.ibenben.zhongdian.system.service.mongodb.MongoChinaCityService;
import xyz.ibenben.zhongdian.system.service.mongodb.MongoChinaProvinceService;
import xyz.ibenben.zhongdian.system.service.mongodb.MongoChinaRegionService;
import xyz.ibenben.zhongdian.system.service.sys.SysUserService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 服务启动执行
 *
 * @author chenjian
 * @since 2016年1月9日
 */
@Slf4j
@Component
public class MyStartupRunner implements CommandLineRunner {
    @Resource
    private ChinaProvinceService chinaProvinceService;
    @Resource
    private ChinaCityService chinaCityService;
    @Resource
    private ChinaRegionService chinaRegionService;
    @Resource
    private SysUserService sysUserService;
    @Resource
    private RedisService redisService;
    @Resource
    private MongoChinaCityService mongoChinaCityService;
    @Resource
    private MongoChinaProvinceService mongoChinaProvinceService;
    @Resource
    private MongoChinaRegionService mongoChinaRegionService;

    @Resource
    private ElasticsearchHouseInfoService elasticsearchHouseInfoService;
    @Resource
    private HouseInfoService houseInfoService;

    /**
     * 服务启动执行，执行加载数据等操作
     *
     * @param args 参数
     */
    @Override
    public void run(String... args) {
        log.info(">>>>>>>>>>>>>>>服务启动执行，执行加载数据等操作<<<<<<<<<<<<<");
        //获取所有省份记录
        List<MongoChinaProvince> pList = mongoChinaProvinceService.selectAll();
        if (pList == null || pList.isEmpty()) {
            pList = new ArrayList<>();
            List<ChinaProvince> chinaProvinces = chinaProvinceService.selectAll();
            if (chinaProvinces != null && !chinaProvinces.isEmpty()) {
                for (ChinaProvince province : chinaProvinces) {
                    MongoChinaProvince mongoChinaProvince = new MongoChinaProvince();
                    BeanUtils.copyProperties(province, mongoChinaProvince);
                    pList.add(mongoChinaProvince);
                }
            }
            mongoChinaProvinceService.saveBatch(pList);
        }
        for (MongoChinaProvince province : pList) {
            redisService.set(province.getProvinceCode().toString(), province.getProvinceName());
            List<MongoChinaCity> list = mongoChinaCityService.findListByProvinceCode(province.getProvinceCode());
            //把城市和省份放到Redis里
            redisService.set("city" + province.getProvinceCode(), list);
        }
        //把省份放到Redis里
        redisService.set("provinceList", pList);

        //获取所有城市记录
        List<MongoChinaCity> cList = mongoChinaCityService.selectAll();
        if (cList == null || cList.isEmpty()) {
            cList = new ArrayList<>();
            List<ChinaCity> chinaCities = chinaCityService.selectAll();
            if (chinaCities != null && !chinaCities.isEmpty()) {
                for (ChinaCity city : chinaCities) {
                    MongoChinaCity mongoChinaCity = new MongoChinaCity();
                    BeanUtils.copyProperties(city, mongoChinaCity);
                    cList.add(mongoChinaCity);
                }
            }
            mongoChinaCityService.saveBatch(cList);
        }
        for (MongoChinaCity city : cList) {
            redisService.set(city.getCityCode().toString(), city.getCityName());
            List<MongoChinaRegion> list = mongoChinaRegionService.findListByCityCode(city.getCityCode());
            //把区县和城市放到Redis里
            redisService.set("region" + city.getCityCode(), list);
        }
        //把城市放到Redis里
        redisService.set("cityList", cList);

        //获取所有区县记录
        List<MongoChinaRegion> rList = mongoChinaRegionService.selectAll();
        if (rList == null || rList.isEmpty()) {
            rList = new ArrayList<>();
            List<ChinaRegion> chinaRegions = chinaRegionService.selectAll();
            if (chinaRegions != null && !chinaRegions.isEmpty()) {
                for (ChinaRegion chinaRegion : chinaRegions) {
                    MongoChinaRegion mongoChinaRegion = new MongoChinaRegion();
                    BeanUtils.copyProperties(chinaRegion, mongoChinaRegion);
                    rList.add(mongoChinaRegion);
                }
            }
            mongoChinaRegionService.saveBatch(rList);
        }
        for (MongoChinaRegion region : rList) {
            //把区县和区县名放到Redis里
            redisService.set(region.getRegionCode().toString(), region.getRegionName());
        }
        //把区县放到Redis里
        redisService.set("regionList", rList);

        //获取所有用户记录
        List<SysUser> users = sysUserService.selectAll();
        for (SysUser user : users) {
            //把用户放到Redis里id是主键
            redisService.set(user.getId().toString(), user.getUsername());
        }

        List<ElasticsearchHouseInfo> elasticsearchHouseInfos = elasticsearchHouseInfoService.selectAll();
        if (elasticsearchHouseInfos == null || elasticsearchHouseInfos.isEmpty()) {
            //把所有房屋信息放入es
            List<HouseInfo> houseInfos = houseInfoService.selectAll();
            for (HouseInfo houseInfo : houseInfos) {
                elasticsearchHouseInfoService.saveEntity(houseInfo, "houseInfo");
            }
        }
    }

}
