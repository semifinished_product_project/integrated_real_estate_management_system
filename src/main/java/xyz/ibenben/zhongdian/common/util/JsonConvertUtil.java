package xyz.ibenben.zhongdian.common.util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * json与String类型转换通用方法
 * 字符串与json字符的转换类
 * 用于解析json字符和互相转换的工具，包括Json转换成String、Json中的特定字段转换成String等方法
 *
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
@SuppressWarnings("unchecked")
public class JsonConvertUtil {
    private JsonConvertUtil() {
    }

    /**
     * Json转换成String
     *
     * @param jsonObj 参数
     * @return 返回值
     */
    public static String jsonToString(JSONObject jsonObj) {
        return jsonObj.toString();
    }

    /**
     * Json中的特定字段转换成String
     *
     * @param jsonObj 参数
     * @return 返回值
     */
    public static String jsonToStringKey(JSONObject jsonObj, String key) {
        JSONObject jsonObject = JSONObject.fromObject(jsonObj);
        return jsonObject.get(key).toString();
    }

    /**
     * Json中的特定字段转换成String
     *
     * @param jsonObj 参数
     * @return 返回值
     */
    public static JSONObject jsonToJsonKey(JSONObject jsonObj, String key) {
        JSONObject jsonObject = JSONObject.fromObject(jsonObj);
        return (JSONObject) jsonObject.get(key);
    }

    /**
     * Json转换成Entity
     *
     * @param jsonObj 参数
     * @param clazz   参数
     * @return 返回值
     */
    public static <T> T jsonToEntity(JSONObject jsonObj, Class<T> clazz) {
        return (T) JSONObject.toBean(jsonObj, clazz);
    }

    /**
     * Entity装换成Json
     *
     * @return 返回值
     */
    public static JSONObject entityToJson(Object entity) {
        return JSONObject.fromObject(entity);
    }

    /**
     * String转换成Json
     *
     * @param str 参数
     * @return 返回值
     */
    public static JSONObject stringToJson(String str) {
        return JSONObject.fromObject(str);
    }

    /**
     * Json转换成Entity
     *
     * @param clazz 参数
     * @return 返回值
     */
    public static <T> T stringToEntity(String str, Class<T> clazz) {
        JSONObject obj = JSONObject.fromObject(str);
        return (T) JSONObject.toBean(obj, clazz);
    }

    /**
     * 数组转换成Json
     *
     * @param object 参数
     * @return 返回值
     */
    public static String array2json(Object object) {
        JSONArray jsonArray = JSONArray.fromObject(object);
        return jsonArray.toString();

    }

    /**
     * 时间格式化通用处理
     * 作者：Daniel
     *
     * @param jsonObject  参数
     * @param description 参数
     * @param obj         参数
     */
    public static void jsonDateFormatArray(JSONObject jsonObject, String description, Object obj) {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(java.util.Date.class, new JsonDateValueProcessor());
        jsonConfig.registerJsonValueProcessor(Boolean.class, new JsonDateValueProcessor());
        JSONArray jobj = JSONArray.fromObject(obj, jsonConfig);
        jsonObject.put(description, jobj);
    }

    /**
     * 时间格式化通用处理
     * 作者：Daniel
     *
     * @param jsonObject  参数
     * @param description 参数
     * @param obj         参数
     */
    public static void jsonDateFormatObject(JSONObject jsonObject, String description, Object obj) {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(java.util.Date.class, new JsonDateValueProcessor());
        jsonConfig.registerJsonValueProcessor(Boolean.class, new JsonDateValueProcessor());
        JSONObject jobj = JSONObject.fromObject(obj, jsonConfig);
        jsonObject.put(description, jobj);
    }
}

