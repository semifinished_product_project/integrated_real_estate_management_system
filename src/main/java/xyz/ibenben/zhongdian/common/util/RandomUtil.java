package xyz.ibenben.zhongdian.common.util;

import java.util.Random;

/**
 * 随机数util
 * 对一些需要随机生成的，比如id，随机数等等
 * 规定了其中的规则后可以使用此类来做随机数处理
 *
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
public class RandomUtil {
    private RandomUtil() {
    }

    /**
     * 生成随机id
     *
     * @return 返回值
     */
    public static String getRandomId(String prefix) {
        String timeMiddle = DateUtils.getCurrentTime(null, "yyyyMMddHHmmss");
        return prefix + timeMiddle + getRandomNumber();
    }

    /**
     * 生成三位随机数
     *
     * @return 返回值
     */
    private static String getRandomNumber() {
        Random r = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            sb.append((char) ('0' + r.nextInt(10)));
        }
        return sb.toString();
    }

    /**
     * 生成N位随机数
     *
     * @return 返回值
     */
    public static String getRandomNumber(int n) {
        Random r = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append((char) ('0' + r.nextInt(10)));
        }
        return sb.toString();
    }

    /**
     * 生成N位大小写字母和数字的随机组合的随机数
     *
     * @param length 参数
     * @return 返回值
     */
    public static String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 获取随机颜色值
     *
     * @return 返回值
     */
    public static String getRandomColor() {
        String red; //红色
        String green; //绿色
        String blue; //蓝色
        //生成随机对象
        Random random = new Random();
        int max = 256;
        int min = 200;
        red = Integer.toHexString(random.nextInt(max) % (max - min + 1) + min).toUpperCase(); //生成红色颜色代码
        green = Integer.toHexString(random.nextInt(max) % (max - min + 1) + min).toUpperCase(); //生成绿色颜色代码
        blue = Integer.toHexString(random.nextInt(max) % (max - min + 1) + min).toUpperCase();    //生成蓝色颜色代码
        red = red.length() == 1 ? "0" + red : red;     //判断红色代码的位数
        green = green.length() == 1 ? "0" + green : green; //判断绿色代码的位数
        blue = blue.length() == 1 ? "0" + blue : blue; //判断蓝色代码的位数
        //生成十六进制颜色值
        return "#" + red + green + blue;
    }
}
