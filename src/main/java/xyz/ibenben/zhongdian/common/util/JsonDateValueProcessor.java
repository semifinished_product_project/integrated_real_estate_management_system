package xyz.ibenben.zhongdian.common.util;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Json处理日期的工具类
 * 根据一个Format来处理需要转换的对象里的日期到页面上是对象的问题
 * 使用此类可以把日期对象直接怼成字符串
 *
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
public class JsonDateValueProcessor implements JsonValueProcessor {
    private String format = "yyyy-MM-dd HH:mm:ss";

    /**
     * 构造函数
     */
    public JsonDateValueProcessor() {
        super();
    }

    /**
     * 带参数的构造函数
     *
     * @param format 参数
     */
    public JsonDateValueProcessor(String format) {
        super();
        this.format = format;
    }

    /**
     * 处理数组类型
     *
     * @param paramObject     参数
     * @param paramJsonConfig 参数
     * @return 返回值
     */
    @Override
    public Object processArrayValue(Object paramObject,
                                    JsonConfig paramJsonConfig) {
        return process(paramObject);
    }

    /**
     * 处理对象类型
     *
     * @param paramString     参数
     * @param paramObject     参数
     * @param paramJsonConfig 参数
     * @return 返回值
     */
    @Override
    public Object processObjectValue(String paramString, Object paramObject,
                                     JsonConfig paramJsonConfig) {
        return process(paramObject);
    }

    /**
     * 处理数据
     *
     * @param value 参数
     * @return 返回值
     */
    private Object process(Object value) {
        if (value instanceof Date) {
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.CHINA);
            return sdf.format((Date) value);
        } else if (value instanceof Boolean) {
            return String.valueOf(value);
        }
        if (value == null) {
            return "";
        } else {
            return value.toString();
        }
    }

}
