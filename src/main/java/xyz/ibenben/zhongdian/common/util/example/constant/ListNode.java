package xyz.ibenben.zhongdian.common.util.example.constant;

import lombok.ToString;

/**
 * 链表
 * Definition for singly-linked list.
 */
@ToString
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int x) {
        val = x;
    }

}
