package xyz.ibenben.zhongdian.common.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import xyz.ibenben.zhongdian.common.exception.ExceptionEnum;
import xyz.ibenben.zhongdian.common.exception.MyException;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * 文件工具类
 * 对文件的读写处理类，项目中用到的上传和下载都是用此类处理
 * 其中包含添加图片、更新图片等方法
 *
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
public class FileUtil {
    private FileUtil() {
    }

    /**
     * 添加图片
     *
     * @param myfile  参数
     * @param request 参数
     * @return 返回值
     */
    public static String addPhoto(MultipartFile myfile, HttpServletRequest request) {
        return processFile(myfile, request);
    }

    /**
     * 更新图片
     *
     * @param myfile  参数
     * @param image   参数
     * @param request 参数
     * @return 返回值
     */
    public static String upload(MultipartFile myfile, String image, HttpServletRequest request) {
        if (StringUtils.isBlank(myfile.getOriginalFilename())) {
            return "";
        }
        File file = new File(image);
        if (file.exists()) {
            Boolean result = file.delete();
            if (!result) {
                throw new MyException(ExceptionEnum.NODELETEEXCEPTION, new Exception());
            }
        }
        return processFile(myfile, request);
    }

    /**
     * 处理文件
     *
     * @param myfile  参数
     * @param request 参数
     * @return 返回值
     */
    private static String processFile(MultipartFile myfile, HttpServletRequest request) {
        String uuid = UUID.randomUUID().toString();
        if (StringUtils.isBlank(myfile.getOriginalFilename())) {
            return "";
        }
        String realPath = request.getSession().getServletContext().getRealPath("/images/");
        File path = new File(realPath);
        if (!path.exists()) {
            Boolean result = path.mkdirs();
            if (!result) {
                throw new MyException(ExceptionEnum.NOFILEEXCEPTION, new Exception());
            }
        }
        return processFileName(myfile, realPath, uuid);
    }

    /**
     * 处理文件名称
     *
     * @param myfile   参数
     * @param realPath 参数
     * @param uuid     参数
     * @return 返回值
     */
    private static String processFileName(MultipartFile myfile, String realPath, String uuid) {
        // 获取文件名
        String fileName = myfile.getOriginalFilename();
        // 获取文件的后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf('.'));
        File dest = new File(realPath + uuid + suffixName);
        try {
            FileUtils.copyInputStreamToFile(myfile.getInputStream(), dest);
        } catch (IOException e) {
            throw new MyException(ExceptionEnum.INPUTEXCEPTION, e);
        }
        return "images/" + uuid + suffixName;
    }
}
