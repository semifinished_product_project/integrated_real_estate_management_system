package xyz.ibenben.zhongdian.common.shiro;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;

/**
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
public class PasswordHelper {
    //private RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator()
    //private String salt = "2AvVhdsgUs0FSA3SDFAdag=="

    /**
     * 根据加密类型和机密次数生成新的密码
     *
     * @param user 参数
     */
    public void encryptPassword(SysUser user) {
        //String salt=randomNumberGenerator.nextBytes().toHex()
        String newPassword = new SimpleHash(Constants.ALGORITHMNAME, user.getPassword(),
                ByteSource.Util.bytes(user.getUsername()), Constants.HASHITERATIONS).toHex();
        //String newPassword = new SimpleHash(algorithmName, sysUser.getPassword()).toHex()
        user.setPassword(newPassword);

    }

}
