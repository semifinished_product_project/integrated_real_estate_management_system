package xyz.ibenben.zhongdian.common.annotation;

import java.lang.annotation.*;

/**
 * 自定义注解 拦截Controller
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SystemControllerLog {
    /**
     * 描述
     *
     * @return 返回值
     */
    String description() default "";
}
