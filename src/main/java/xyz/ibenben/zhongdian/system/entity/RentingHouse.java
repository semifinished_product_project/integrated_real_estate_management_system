package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.PayTypeEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 房屋租赁实体类
 * 记录该表记录了房屋主键，房屋所有人等字段
 * 表名是renting_house
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Setter
@Getter
@ToString
public class RentingHouse extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 4790488995597270526L;

    /**
     * 房屋主键
     */
    @Column
    private Long houseId;

    /**
     * 房屋所有人
     */
    @Column
    private String owner;

    /**
     * 出租价格
     */
    @Column
    private BigDecimal price;

    /**
     * 支付类型
     */
    @Column
    private PayTypeEnum priceType;

    /**
     * 租金包含费用
     */
    @Column
    private String includeService;

    /**
     * 联系电话
     */
    @Column
    private String phone;

    /**
     * 装修风格
     */
    @Column
    private String fitment;

    /**
     * 提供服务
     */
    @Column
    private String supply;

    /**
     * 标题
     */
    @Column
    private String title;

    /**
     * 房源描述
     */
    @Column
    private String description;

    /**
     * 房屋信息
     */
    @Transient
    private HouseInfo info;

    /**
     * 房屋图片
     */
    @Transient
    private List<HouseImage> list;

}
