package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * 站内信记录实体类
 * 记录该表记录了发送者，接收者等字段
 * 表名是inner_mail_record
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Getter
@Setter
@ToString
public class InnerMailRecord extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -1847703885928213412L;

    /**
     * 发送者
     */
    @Column
    private Long imId;

    /**
     * 接收者
     */
    @Column
    private Long toId;

    /**
     * 接收者名称
     */
    @Transient
    private String toUserName;

    /**
     * 是否已读 0未读 1已读
     */
    @Column
    private Boolean isRead;

    /**
     * 发送时间
     */
    @Column
    private Date sendTime;

}
