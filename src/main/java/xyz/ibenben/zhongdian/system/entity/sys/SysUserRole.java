package xyz.ibenben.zhongdian.system.entity.sys;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * 系统用户角色实体类（多对多关系表）
 * 记录该表记录了角色主键，用户主键等字段
 * 表名是sys_user_role
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
@Entity
public class SysUserRole implements Serializable {
    private static final long serialVersionUID = -916411139749530670L;

    /**
     * 用户主键
     */
    @Id
    @Column(name = "user_id")
    private Long userId;

    /**
     * 角色主键
     */
    @Id
    @Column(name = "role_id")
    private Long roleId;

    @Transient
    private String roleIds;
}