package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.MailTypeEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 站内信实体类
 * 记录该表记录了标题，内容等字段
 * 表名是inner_mail
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Getter
@Setter
@ToString
public class InnerMail extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -1847703885928213412L;

    /**
     * 标题
     */
    @NotEmpty(message = "标题不能为空")
    @Column
    private String title;

    /**
     * 内容
     */
    @NotEmpty(message = "内容不能为空")
    @Column
    private String content;

    /**
     * 站内信类型 1后台公告 2前台公告 3后台私信 4前台私信
     */
    @NotNull(message = "站内信类型不能为空")
    @Enumerated
    @Column
    private MailTypeEnum type;

    /**
     * 发送者
     */
    @Column
    private Long fromId;

    /**
     * 接收者
     */
    @Transient
    private Long toId;

    /**
     * 发送者名称
     */
    @Transient
    private String fromUserName;

    /**
     * 接收者名称
     */
    @Transient
    private String toUserName;

    /**
     * 是否已读 0未读 1已读
     */
    @Transient
    private Boolean isRead;

    /**
     * 发送时间
     */
    @Transient
    private Date sendTime;

    /**
     * 发送内容
     */
    @Transient
    private List<InnerMailRecord> list;

}
