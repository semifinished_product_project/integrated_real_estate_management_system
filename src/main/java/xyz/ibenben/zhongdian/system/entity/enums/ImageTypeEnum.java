package xyz.ibenben.zhongdian.system.entity.enums;

import xyz.ibenben.zhongdian.common.converter.BaseEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * 房屋图像类型枚举类
 * 包括房屋信息，房产证等
 * 使用枚举类可以有效的节省代码行数，调整系统的耦合度
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public enum ImageTypeEnum implements BaseEnum<Integer> {
    HOUSE(1, "房屋信息"), CERTIFICATE(2, "房产证"), BOOKING(3, "预售信息"), RENTING(4, "租赁信息");

    /* Map对象 */
    private static Map<Integer, ImageTypeEnum> valueMap = new HashMap<>();

    static {
        for (ImageTypeEnum mail : ImageTypeEnum.values()) {
            valueMap.put(mail.getValue(), mail);
        }
    }

    /* 值 */
    private int value;
    /* 文字 */
    private String text;

    /**
     * 构造方法
     *
     * @param value
     * @param text
     */
    ImageTypeEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取值
     *
     * @param value 参数
     * @return 返回值
     */
    public static ImageTypeEnum getByValue(int value) {
        ImageTypeEnum result = valueMap.get(value);
        if (result == null) {
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }

    /**
     * 获取文字
     *
     * @param text 参数
     * @return 返回值
     */
    public static ImageTypeEnum getByText(String text) {
        for (ImageTypeEnum e : values()) {
            if (e.getText().equals(text)) {
                return e;
            }
        }
        return null;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public Integer getValue() {
        return value;
    }

}