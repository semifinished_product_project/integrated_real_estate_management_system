package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.RightTypeEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 他项权利摘要实体类
 * 记录该表记录了拥有者，权利人等字段
 * 表名是other_right_digest
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Setter
@Getter
@ToString
public class OtherRightDigest extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 8416303275004427185L;

    /**
     * 拥有者
     */
    @Column
    private Long ownerId;

    /**
     * 权利人
     */
    @Column
    private String righter;

    /**
     * 权利种类
     */
    @Column
    private RightTypeEnum type;

    /**
     * 权利范围
     */
    @Column
    private String ranges;

    /**
     * 权利价值
     */
    @Column
    private BigDecimal price;

    /**
     * 设定日期
     */
    @Column
    private Date setDate;

    /**
     * 约定期限
     */
    @Column
    private Date dateLimit;

    /**
     * 注销日期
     */
    @Column
    private Date logoutDate;

    /**
     * 从拥有者主键
     */
    @Transient
    private boolean fromOwnerId = false;

}
