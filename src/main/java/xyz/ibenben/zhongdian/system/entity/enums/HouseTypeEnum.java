package xyz.ibenben.zhongdian.system.entity.enums;

import xyz.ibenben.zhongdian.common.converter.BaseEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * 房屋类型枚举类
 * 包括预售中，租赁中等
 * 使用枚举类可以有效的节省代码行数，调整系统的耦合度
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public enum HouseTypeEnum implements BaseEnum<Integer> {
    BOOKING(1, "预售中"), RENTING(2, "租赁中"), PRIVATE(3, "待售中");

    /* Map对象 */
    private static Map<Integer, HouseTypeEnum> valueMap = new HashMap<>();

    static {
        for (HouseTypeEnum mail : HouseTypeEnum.values()) {
            valueMap.put(mail.getValue(), mail);
        }
    }

    /* 值 */
    private int value;
    /* 文字 */
    private String text;

    /**
     * 构造方法
     *
     * @param value
     * @param text
     */
    HouseTypeEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取值
     *
     * @param value 参数
     * @return 返回值
     */
    public static HouseTypeEnum getByValue(int value) {
        HouseTypeEnum result = valueMap.get(value);
        if (result == null) {
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }

    /**
     * 获取文字
     *
     * @param text 参数
     * @return 返回值
     */
    public static HouseTypeEnum getByText(String text) {
        for (HouseTypeEnum e : values()) {
            if (e.getText().equals(text)) {
                return e;
            }
        }
        return null;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public Integer getValue() {
        return value;
    }

}