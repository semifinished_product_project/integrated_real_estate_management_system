package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.SuggestTypeEnum;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * 日志实体类
 * 记录该表记录了描述，方法等字段
 * 表名是log
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@Entity
@ToString
public class SuggestInfo extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 4254130484943935732L;

    /**
     * 用户主键
     */
    @Column
    private Long userId;

    /**
     * 用户信息
     */
    @Transient
    private SysUser sysUser;

    /**
     * 评分
     */
    @Column
    private Double score;

    /**
     * 联系方式
     */
    @Column
    private String phone;

    /**
     * 电子邮箱
     */
    @Column
    private String email;

    /**
     * 建议或意见
     */
    @Column
    private String content;

    /**
     * 类型 0-意见 1-建议
     */
    @Column
    private SuggestTypeEnum type;
}
