package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.DealRecordTypeEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 成交记录信息实体类
 * 记录该表记录了成交记信息，购买人等字段
 * 表名是deal_record_info
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Getter
@Setter
@ToString
public class DealRecordInfo extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 5441997212327447070L;

    /**
     * 房屋主键
     */
    @Column
    private Long houseId;

    /**
     * 房屋信息
     */
    @Transient
    private HouseInfo houseInfo;

    /**
     * 购买人
     */
    @NotEmpty(message = "购买人不能为空")
    @Column
    private String buyer;

    /**
     * 联系电话
     */
    @NotNull(message = "联系电话不能为空")
    @Column
    private String phone;

    /**
     * 身份证
     */
    @NotNull(message = "身份证不能为空")
    @Column
    private String idCard;

    /**
     * 地址
     */
    @NotNull(message = "地址不能为空")
    @Column
    private String address;

    /**
     * 成交价格
     */
    @NotNull(message = "成交价格不能为空")
    @Column
    private BigDecimal price;

    /**
     * 成交时间
     */
    @NotNull(message = "成交时间不能为空")
    @Column
    private Date dealTime;

    /**
     * 成交类型 0-预收 1-租赁 2-其他
     */
    @NotNull(message = "成交类型不能为空")
    @Column
    private DealRecordTypeEnum type;

    /**
     * 折扣主键
     */
    @Column
    private Long discountId;

    /**
     * 折扣信息
     */
    @Transient
    private DiscountInfo discountInfo;

    /**
     * 所在省
     */
    @NotNull(message = "所在省不能为空")
    @Column
    private Long province;

    /**
     * 所在市
     */
    @NotNull(message = "所在市不能为空")
    @Column
    private Long city;

    /**
     * 所在区
     */
    @NotNull(message = "所在区不能为空")
    @Column
    private Long region;

}
