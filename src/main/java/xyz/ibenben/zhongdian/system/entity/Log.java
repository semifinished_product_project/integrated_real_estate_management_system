package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * 日志实体类
 * 记录该表记录了描述，方法等字段
 * 表名是log
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@Entity
@ToString
public class Log extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 4254130484943935732L;

    /**
     * 描述
     */
    @Column
    private String description;

    /**
     * 方法
     */
    @Column
    private String method;

    /**
     * 类型
     */
    @Column
    private String type;

    /**
     * 请求IP地址
     */
    @Column
    private String requestIp;

    /**
     * 异常代码
     */
    @Column
    private String exceptionCode;

    /**
     * 异常信息
     */
    @Column
    private String exceptionDetail;

    /**
     * 参数
     */
    @Column
    private String params;

}
