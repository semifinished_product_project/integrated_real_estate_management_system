package xyz.ibenben.zhongdian.system.entity.enums;

import xyz.ibenben.zhongdian.common.converter.BaseEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * 房屋结构枚举类
 * 包括钢筋混凝土，砖混结构等
 * 使用枚举类可以有效的节省代码行数，调整系统的耦合度
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public enum HouseStructureEnum implements BaseEnum<Integer> {
    FERROCONCRETE(1, "钢筋混凝土"), BRICK(2, "砖混结构"), TIMBERWORK(3, "木结构"), STEELWORK(4, "钢结构");

    /* Map对象 */
    private static Map<Integer, HouseStructureEnum> valueMap = new HashMap<>();

    static {
        for (HouseStructureEnum mail : HouseStructureEnum.values()) {
            valueMap.put(mail.getValue(), mail);
        }
    }

    /* 值 */
    private int value;
    /* 文字 */
    private String text;

    /**
     * 构造方法
     *
     * @param value
     * @param text
     */
    HouseStructureEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取值
     *
     * @param value 参数
     * @return 返回值
     */
    public static HouseStructureEnum getByValue(int value) {
        HouseStructureEnum result = valueMap.get(value);
        if (result == null) {
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }

    /**
     * 获取文字
     *
     * @param text 参数
     * @return 返回值
     */
    public static HouseStructureEnum getByText(String text) {
        for (HouseStructureEnum e : values()) {
            if (e.getText().equals(text)) {
                return e;
            }
        }
        return null;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public Integer getValue() {
        return value;
    }

}