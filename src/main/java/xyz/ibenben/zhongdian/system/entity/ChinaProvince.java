package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * 省份实体类
 * 记录该表记录了省份代码，省份名称等字段
 * 表名是china_province
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Getter
@Setter
@ToString
public class ChinaProvince extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 5575289311784470796L;

    /**
     * 省份代码
     */
    @Column
    private Long provinceCode;

    /**
     * 省份名称
     */
    @Column
    private String provinceName;

}
