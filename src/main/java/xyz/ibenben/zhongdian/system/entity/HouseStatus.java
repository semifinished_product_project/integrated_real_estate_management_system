package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.HouseStructureEnum;
import xyz.ibenben.zhongdian.system.entity.enums.StatusTypeEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 房屋状态实体类
 * 记录该表记录了拥有者，厅数量等字段
 * 表名是house_status
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Getter
@Setter
@ToString
public class HouseStatus extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1786551986601811472L;

    /**
     * 拥有者
     */
    @Column
    private Long ownerId;

    /**
     * 厅数量
     */
    @Column
    private String hitNumber;

    /**
     * 室数量
     */
    @Column
    private String roomNumber;

    /**
     * 房屋结构
     */
    @Column
    private HouseStructureEnum structure;

    /**
     * 总楼层
     */
    @Column
    private Integer houseTotalLayer;

    /**
     * 所在楼层
     */
    @Column
    private Integer houseInLayer;

    /**
     * 建筑面积
     */
    @Column
    private BigDecimal coveredArea;

    /**
     * 使用面积
     */
    @Column
    private BigDecimal useableArea;

    /**
     * 设计目的
     */
    @Column
    private String designPurpose;

    /**
     * 状态类型
     */
    @Column
    private StatusTypeEnum type;

    /**
     * 从拥有者主键
     */
    @Transient
    private boolean fromOwnerId = false;

}
