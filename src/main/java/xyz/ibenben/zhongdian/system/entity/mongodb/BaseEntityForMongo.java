package xyz.ibenben.zhongdian.system.entity.mongodb;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Date;

/**
 * 基础实体类
 * 数据库里统一设计id为自增
 * 每个数据表里都有创建时间，创建人，更新时间，更新人，删除标识，删除时间，删除人等字段
 * 统一放在这个基类里处理，其他实体类只需要继承此类，就可以免除重复代码
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
@ToString
public class BaseEntityForMongo implements Serializable {
    private static final long serialVersionUID = -4173243207799136289L;

    /**
     * 主键
     */
    @Field
    private Long id;

    /**
     * 创建时间
     */
    @Field
    private Date createTime;

    /**
     * 创建人
     */
    @Field
    private Long createId;

    /**
     * 更新时间
     */
    @Field
    private Date updateTime;

    /**
     * 更新人
     */
    @Field
    private Long updateId;

    /**
     * 删除标识
     */
    @Field
    private Integer delFlag = 0;

    /**
     * 删除时间
     */
    @Field
    private Date delTime;

    /**
     * 删除人
     */
    @Field
    private Long delId;

}
