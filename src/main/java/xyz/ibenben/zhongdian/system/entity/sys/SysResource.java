package xyz.ibenben.zhongdian.system.entity.sys;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class SysResource {

    private Long id;
    /**
     * 资源名称
     */
    private String name;

    /**
     * 父资源
     */
    private Long pId;

    /**
     * 是否选中
     */
    private String checked;

    /**
     * 是否leaf
     */
    private Boolean isLeaf = false;
}
