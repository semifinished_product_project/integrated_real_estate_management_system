package xyz.ibenben.zhongdian.system.entity.mongodb;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * 城市实体类
 * 记录该表记录了省份代码，城市代码等字段
 * 表名是china_city
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Getter
@Setter
@ToString
public class MongoChinaCity extends BaseEntityForMongo implements Serializable {
    private static final long serialVersionUID = 7385041158809959066L;

    /**
     * 省份代码
     */
    @Field
    private Long provinceCode;

    /**
     * 城市代码
     */
    @Field
    private Long cityCode;

    /**
     * 城市名称
     */
    @Field
    private String cityName;

}
