package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * 房屋信息实体类
 * 记录该表记录了房屋名称，室数量路径等字段
 * 表名是house_info
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Entity
@Getter
@Setter
@ToString
public class DiscountInfo extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 5441997212327447070L;

    /**
     * 折扣
     */
    @Column
    private Double discountPercent;

    /**
     * 公司主键
     */
    @Column
    private Long companyId;

    /**
     * 公司信息
     */
    @Transient
    private CompanyInfo companyInfo;

}
