package xyz.ibenben.zhongdian.system.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import xyz.ibenben.zhongdian.common.annotation.SystemControllerLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.StuffInfo;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;
import xyz.ibenben.zhongdian.system.form.StuffInfoForm;
import xyz.ibenben.zhongdian.system.service.RentingHouseService;
import xyz.ibenben.zhongdian.system.service.StuffInfoService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

/**
 * 租赁物品控制类
 * 提供了一些基本的服务，如初始化、获取租赁物品列表等方法。
 * spring mvc思想 通过controller来调用service里的方法，
 * service里的方法再通过调用Dao来实现对数据的操作
 * 返回值统统是String类型的返回页面有统一的安排方便代码提取
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Slf4j
@Controller
@SessionAttributes("stuffInfoForm")
@RequestMapping("/stuffInfo")
@SuppressWarnings(value = "unchecked")
public class StuffInfoController extends BaseController {
    /**
     * 实体名称
     */
    private static final String ENTITYNAME = "stuffInfo";

    @Resource
    private StuffInfoService stuffInfoService;
    @Resource
    private RentingHouseService rentingHouseService;

    /**
     * 初始化
     *
     * @param map           参数
     * @param stuffInfoForm 条件参数
     */
    @ModelAttribute
    public void init(Model map, @ModelAttribute StuffInfoForm stuffInfoForm, HttpSession session) {
        StuffInfoForm newStuffInfoForm = new StuffInfoForm();
        if (stuffInfoForm != null) {
            newStuffInfoForm = stuffInfoForm;
        }
        SysUser user = (SysUser) session.getAttribute(Constants.SESSION);
        if (user != null && user.getSysRole() != null) {
            newStuffInfoForm.setCompanyId(user.getSysRole().getCompanyId());
        }
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), newStuffInfoForm);
    }

    /**
     * 获取租赁物品信息列表
     *
     * @param map           参数
     * @param stuffInfoForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/getAll")
    @SystemControllerLog(description = "获取租赁物品信息列表")
    public String getAll(Model map, @ModelAttribute StuffInfoForm stuffInfoForm) {
        log.info("getAll param StuffInfoForm: {}", stuffInfoForm.toString());
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), stuffInfoForm);
        Page<StuffInfo> page = PageHelper.startPage(stuffInfoForm.getPageIndex(), stuffInfoForm.getPageSize());
        List<StuffInfo> list = stuffInfoService.findAll(stuffInfoForm);
        map.addAttribute(String.format(Constants.LISTENTITY, ENTITYNAME), list);
        map.addAttribute(Constants.PI, page.toPageInfo());
        return String.format(Constants.LISTPAGE, ENTITYNAME);
    }

    /**
     * 删除租赁物品信息记录
     *
     * @param id      主键
     * @param request 请求
     * @return 页面
     */
    @RequestMapping(value = "/delete")
    @SystemControllerLog(description = "删除租赁物品信息记录")
    public String delete(@RequestParam("id") Long id, HttpServletRequest request, SessionStatus status) {
        stuffInfoService.delete(id, request);
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 删除租赁物品信息列表
     *
     * @param stuffInfoForm 条件参数
     * @param request       请求
     * @return 页面
     */
    @RequestMapping("/deleteAll")
    @SystemControllerLog(description = "删除租赁物品信息列表")
    public String deleteAll(@ModelAttribute StuffInfoForm stuffInfoForm,
                            HttpServletRequest request, SessionStatus status) {
        if (stuffInfoForm.getSelectIds() != null) {
            String[] ids = stuffInfoForm.getSelectIds().split(Constants.COMMA);
            for (String id : ids) {
                stuffInfoService.delete(Long.parseLong(id), request);
            }
        }
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 打开租赁物品信息添加页面
     *
     * @param map 参数
     * @return 页面
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开租赁物品信息添加页面")
    public String openAddPage(Model map, @RequestParam("id") Long id) {
        StuffInfo stuffInfo = new StuffInfo();
        stuffInfo.setRentingId(id);
        map.addAttribute(ENTITYNAME, stuffInfo);
        map.addAttribute("rentingHouse", rentingHouseService.selectByKey(id));
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 保存租赁物品信息添加记录
     *
     * @param stuffInfo     实体
     * @param bindingResult 校验
     * @param request       请求
     * @return 页面
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @SystemControllerLog(description = "保存租赁物品信息添加记录")
    public String postAddMessage(@Valid @ModelAttribute(ENTITYNAME) StuffInfo stuffInfo,
                                 BindingResult bindingResult, HttpServletRequest request,
                                 Model map, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            stuffInfoService.save(stuffInfo, request);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        } else {
            map.addAttribute("rentingHouse", rentingHouseService.selectByKey(stuffInfo.getRentingId()));
            return String.format(Constants.ADDPAGE, ENTITYNAME);
        }
    }

    /**
     * 打开租赁物品信息修改页面
     *
     * @param map 参数
     * @param id  主键
     * @return 页面
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开租赁物品信息修改页面")
    public String openEditPage(Model map, @RequestParam("id") Long id) {
        map.addAttribute(ENTITYNAME, stuffInfoService.selectByKey(id));
        return String.format(Constants.EDITPAGE, ENTITYNAME);
    }

    /**
     * 更新租赁物品信息修改记录
     *
     * @param stuffInfo     实体
     * @param bindingResult 校验
     * @param request       请求
     * @return 页面
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @SystemControllerLog(description = "更新租赁物品信息修改记录")
    public String postEditMessage(@Valid @ModelAttribute(ENTITYNAME) StuffInfo stuffInfo,
                                  BindingResult bindingResult, HttpServletRequest request, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            stuffInfoService.updateNotNull(stuffInfo, request);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        } else {
            return String.format(Constants.EDITPAGE, ENTITYNAME);
        }
    }

    /**
     * 浏览租赁物品信息记录
     *
     * @param map           参数
     * @param id            主键
     * @param stuffInfoForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    @SystemControllerLog(description = "浏览租赁物品信息记录")
    public String openViewPage(Model map, @RequestParam("id") Long id,
                               @ModelAttribute StuffInfoForm stuffInfoForm) {
        StuffInfo info = stuffInfoService.findByKey(id);
        map.addAttribute(ENTITYNAME, info);
        return String.format(Constants.VIEWPAGE, ENTITYNAME);
    }

}

