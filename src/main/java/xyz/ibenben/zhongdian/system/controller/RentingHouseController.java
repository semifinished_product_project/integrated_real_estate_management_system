package xyz.ibenben.zhongdian.system.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import xyz.ibenben.zhongdian.common.annotation.SystemControllerLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.RentingHouse;
import xyz.ibenben.zhongdian.system.entity.StuffInfo;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;
import xyz.ibenben.zhongdian.system.form.RentingHouseForm;
import xyz.ibenben.zhongdian.system.service.HouseInfoService;
import xyz.ibenben.zhongdian.system.service.RentingHouseService;
import xyz.ibenben.zhongdian.system.service.StuffInfoService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

/**
 * 租赁房屋控制类
 * 提供了一些基本的服务，如初始化、获取房屋租赁列表等方法。
 * spring mvc思想 通过controller来调用service里的方法，
 * service里的方法再通过调用Dao来实现对数据的操作
 * 返回值统统是String类型的返回页面有统一的安排方便代码提取
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Controller
@SessionAttributes("chatRecordForm")
@RequestMapping("/rentingHouse")
public class RentingHouseController extends BaseController {
    /**
     * 实体名称
     */
    private static final String ENTITYNAME = "rentingHouse";

    @Resource
    private RentingHouseService rentingHouseService;
    @Resource
    private HouseInfoService houseInfoService;
    @Resource
    private StuffInfoService stuffInfoService;

    /**
     * 初始化
     *
     * @param map              参数
     * @param rentingHouseForm 条件参数
     */
    @ModelAttribute
    public void init(Model map, HttpServletRequest request, @ModelAttribute RentingHouseForm rentingHouseForm,
                     HttpSession session) {
        //初始化数据
        Long userId = (Long) request.getSession().getAttribute(Constants.SESSIONID);
        map.addAttribute("houseInfoList", houseInfoService.findByCreateIdAndType(userId, 3));
        //初始化条件放到Session里
        RentingHouseForm newRentingHouseForm = new RentingHouseForm();
        if (rentingHouseForm != null) {
            newRentingHouseForm = rentingHouseForm;
        }
        SysUser user = (SysUser) session.getAttribute(Constants.SESSION);
        if (user != null && user.getSysRole() != null) {
            newRentingHouseForm.setCompanyId(user.getSysRole().getCompanyId());
        }
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), newRentingHouseForm);
    }

    /**
     * 获取房屋租赁列表
     *
     * @param map              参数
     * @param rentingHouseForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/getAll")
    @SystemControllerLog(description = "获取房屋租赁列表")
    public String getAll(Model map, @ModelAttribute RentingHouseForm rentingHouseForm) {
        //获取到的条件重新放入session
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), rentingHouseForm);
        //获取页面信息
        Page<RentingHouse> page = PageHelper.startPage(rentingHouseForm.getPageIndex(), rentingHouseForm.getPageSize());
        //获取列表信息
        List<RentingHouse> list = rentingHouseService.findAll(rentingHouseForm);
        map.addAttribute(String.format(Constants.LISTENTITY, ENTITYNAME), list);
        map.addAttribute(Constants.PI, page.toPageInfo());
        return String.format(Constants.LISTPAGE, ENTITYNAME);
    }

    /**
     * 删除房屋租赁记录
     *
     * @param id      租赁主键
     * @param request 请求
     * @return 页面
     */
    @RequestMapping(value = "/delete")
    @SystemControllerLog(description = "删除房屋租赁记录")
    public String delete(@RequestParam("id") Long id, HttpServletRequest request, SessionStatus status) {
        //逻辑删除数据
        rentingHouseService.delete(id, request);
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 删除房屋租赁列表
     *
     * @param rentingHouseForm 条件参数
     * @param request          请求
     * @return 页面
     */
    @RequestMapping("/deleteAll")
    @SystemControllerLog(description = "删除房屋租赁列表")
    public String deleteAll(@ModelAttribute RentingHouseForm rentingHouseForm,
                            HttpServletRequest request, SessionStatus status) {
        //根据选择的ids删除数据
        if (rentingHouseForm.getSelectIds() != null) {
            String[] ids = rentingHouseForm.getSelectIds().split(Constants.COMMA);
            for (String id : ids) {
                rentingHouseService.delete(Long.parseLong(id), request);
            }
        }
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 打开房屋租赁添加页面
     *
     * @param map 参数
     * @return 页面
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开房屋租赁添加页面")
    public String openAddPage(Model map) {
        //创建新的实体到页面
        map.addAttribute(ENTITYNAME, new RentingHouse());
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 保存房屋租赁添加记录
     *
     * @param rentingHouse  实体
     * @param bindingResult 校验
     * @param request       请求
     * @return 页面
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @SystemControllerLog(description = "保存房屋租赁添加记录")
    public String postAddMessage(@Valid @ModelAttribute RentingHouse rentingHouse,
                                 BindingResult bindingResult, HttpServletRequest request, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            //保存实体
            rentingHouseService.save(rentingHouse, request);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        }
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 打开房屋租赁修改页面
     *
     * @param map 参数
     * @param id  租赁主键
     * @return 页面
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开房屋租赁修改页面")
    public String openEditPage(Model map, @RequestParam("id") Long id) {
        //获取实体通过主键
        map.addAttribute(ENTITYNAME, rentingHouseService.findOneWithImage(id));
        return String.format(Constants.EDITPAGE, ENTITYNAME);
    }

    /**
     * 更新房屋租赁修改记录
     *
     * @param rentingHouse  实体
     * @param bindingResult 校验
     * @param request       请求
     * @return 页面
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @SystemControllerLog(description = "更新房屋租赁修改记录")
    public String postEditMessage(@Valid @ModelAttribute RentingHouse rentingHouse,
                                  BindingResult bindingResult, HttpServletRequest request, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            //更新实体
            rentingHouseService.updateNotNull(rentingHouse, request);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        }
        return String.format(Constants.EDITPAGE, ENTITYNAME);
    }

    /**
     * 浏览房屋租赁记录
     *
     * @param map              参数
     * @param id               租赁主键
     * @param rentingHouseForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    @SystemControllerLog(description = "浏览房屋租赁记录")
    public String openViewPage(Model map, @RequestParam("id") Long id,
                               @ModelAttribute RentingHouseForm rentingHouseForm) {
        //获取租赁信息
        map.addAttribute(ENTITYNAME, rentingHouseService.findOneWithImage(id));
        List<StuffInfo> list = stuffInfoService.selectByRentingHouseId(id);
        map.addAttribute("stuffInfoList", list);
        return String.format(Constants.VIEWPAGE, ENTITYNAME);
    }

}

