package xyz.ibenben.zhongdian.system.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import xyz.ibenben.zhongdian.common.annotation.SystemControllerLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.InnerMail;
import xyz.ibenben.zhongdian.system.form.InnerMailForm;
import xyz.ibenben.zhongdian.system.service.InnerMailService;
import xyz.ibenben.zhongdian.system.service.sys.SysUserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * 站内信控制类
 * 提供了一些基本的服务，如初始化、获取站内信列表等方法。
 * spring mvc思想 通过controller来调用service里的方法，
 * service里的方法再通过调用Dao来实现对数据的操作
 * 返回值统统是String类型的返回页面有统一的安排方便代码提取
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Controller
@SessionAttributes("innerMailForm")
@RequestMapping("/innerMail")
public class InnerMailController extends BaseController {
    /**
     * 实体名称
     */
    private static final String ENTITYNAME = "innerMail";

    @Resource
    private InnerMailService innerMailService;
    @Resource
    private SysUserService sysUserService;

    /**
     * 初始化
     *
     * @param map           参数
     * @param innerMailForm 条件参数
     */
    @ModelAttribute
    public void init(Model map, @ModelAttribute InnerMailForm innerMailForm) {
        map.addAttribute("userList", sysUserService.selectAll());
        InnerMailForm newInnerMailForm = new InnerMailForm();
        if (innerMailForm != null) {
            newInnerMailForm = innerMailForm;
        }
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), newInnerMailForm);
    }

    /**
     * 获取站内信列表
     *
     * @param map           参数
     * @param innerMailForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/getAll")
    @SystemControllerLog(description = "获取站内信列表")
    public String getAll(Model map, @ModelAttribute InnerMailForm innerMailForm) {
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), innerMailForm);
        Page<InnerMail> page = PageHelper.startPage(innerMailForm.getPageIndex(), innerMailForm.getPageSize());
        List<InnerMail> list = innerMailService.findAll(innerMailForm);
        map.addAttribute(String.format(Constants.LISTENTITY, ENTITYNAME), list);
        map.addAttribute(Constants.PI, page.toPageInfo());
        return String.format(Constants.LISTPAGE, ENTITYNAME);
    }

    /**
     * 删除站内信记录
     *
     * @param id      主键
     * @param request 请求
     * @return 页面
     */
    @RequestMapping(value = "/delete")
    @SystemControllerLog(description = "删除站内信记录")
    public String delete(@RequestParam(Constants.ID) Long id, HttpServletRequest request, SessionStatus status) {
        innerMailService.delete(id, request);
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 删除站内信列表
     *
     * @param innerMailForm 条件参数
     * @param request       请求
     * @return 页面
     */
    @RequestMapping("/deleteAll")
    @SystemControllerLog(description = "删除站内信列表")
    public String deleteAll(@ModelAttribute InnerMailForm innerMailForm,
                            HttpServletRequest request, SessionStatus status) {
        if (innerMailForm.getSelectIds() != null) {
            String[] ids = innerMailForm.getSelectIds().split(Constants.COMMA);
            for (String id : ids) {
                innerMailService.delete(Long.parseLong(id), request);
            }
        }
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 打开站内信添加页面
     *
     * @param map 参数
     * @return 页面
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开站内信添加页面")
    public String openAddPage(Model map) {
        map.addAttribute(ENTITYNAME, new InnerMail());
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 保存站内信添加记录
     *
     * @param innerMail     实体
     * @param bindingResult 校验
     * @param request       请求
     * @return 页面
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @SystemControllerLog(description = "保存站内信添加记录")
    public String postAddMessage(@Valid @ModelAttribute(ENTITYNAME) InnerMail innerMail,
                                 BindingResult bindingResult, HttpServletRequest request, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            innerMail.setFromId((Long) request.getSession().getAttribute(Constants.SESSIONID));
            innerMailService.save(innerMail, request);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        }
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 浏览站内信记录
     *
     * @param map           参数
     * @param id            主键
     * @param innerMailForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    @SystemControllerLog(description = "浏览站内信记录")
    public String openViewPage(Model map, @RequestParam(Constants.ID) Long id,
                               @ModelAttribute InnerMailForm innerMailForm) {
        map.addAttribute(ENTITYNAME, innerMailService.selectByKeyWithList(id));
        return String.format(Constants.VIEWPAGE, ENTITYNAME);
    }

    /**
     * 获取所有站内信列表
     *
     * @param map           参数
     * @param request       请求
     * @param innerMailForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/allDetail")
    @SystemControllerLog(description = "获取所有站内信列表")
    public String allDetail(Model map, HttpServletRequest request,
                            @ModelAttribute InnerMailForm innerMailForm) {
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), innerMailForm);
        Page<InnerMail> page = PageHelper.startPage(innerMailForm.getPageIndex(), innerMailForm.getPageSize());
        List<InnerMail> list = innerMailService.getAllDetail(innerMailForm.getTitle(), innerMailForm.getIsRead(), request);
        map.addAttribute(String.format(Constants.LISTENTITY, ENTITYNAME), list);
        map.addAttribute(Constants.PI, page.toPageInfo());
        return "innerMail/detailList";
    }

    /**
     * 浏览站内信记录详情
     *
     * @param map     参数
     * @param id      主键
     * @param request 请求
     * @return 页面
     */
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    @SystemControllerLog(description = "浏览站内信记录详情")
    public String openDetailPage(Model map, @RequestParam(Constants.ID) Long id, HttpServletRequest request) {
        map.addAttribute(ENTITYNAME, innerMailService.selectByKeyAndUserId(id, request));
        return "innerMail/detail";
    }

}

