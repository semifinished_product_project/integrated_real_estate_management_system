package xyz.ibenben.zhongdian.system.controller;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.ibenben.zhongdian.common.annotation.SystemControllerLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.common.util.HttpServletResponseUtil;
import xyz.ibenben.zhongdian.system.entity.Reminder;
import xyz.ibenben.zhongdian.system.entity.SuggestInfo;
import xyz.ibenben.zhongdian.system.entity.ajax.AjaxJson;
import xyz.ibenben.zhongdian.system.service.ReminderService;
import xyz.ibenben.zhongdian.system.service.SuggestInfoService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * @author chenjian
 */
@Controller
@RequestMapping(value = "/")
public class FourFunctionController extends BaseController {
    @Resource
    private ReminderService reminderService;
    @Resource
    private SuggestInfoService suggestInfoService;

    /**
     * 打开FAQ页面
     *
     * @return 页面
     */
    @RequestMapping(value = "/faq", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开FAQ页面")
    public String faq() {
        return "fourFunction/faq";
    }

    /**
     * 打开日历页面
     *
     * @return 页面
     */
    @RequestMapping(value = "/calendar", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开日历页面")
    public String calendar() {
        return "fourFunction/calendar";
    }

    /**
     * 打开统计页面
     *
     * @return 页面
     */
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开统计页面")
    public String count() {
        return "fourFunction/count";
    }

    /**
     * 打开投票页面
     *
     * @return 页面
     */
    @RequestMapping(value = "/vote", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开投票页面")
    public String vote(Model map) {
        map.addAttribute("suggestInfo", new SuggestInfo());
        return "fourFunction/vote";

    }

    /**
     * 保存意见及建议添加记录
     *
     * @param suggestInfo 实体
     * @param request     请求
     * @param response    响应
     * @return 页面
     */
    @RequestMapping(value = "/addSuggestInfo", method = RequestMethod.POST)
    @SystemControllerLog(description = "保存意见及建议添加记录")
    @ResponseBody
    public String addSuggestInfo(SuggestInfo suggestInfo, HttpServletRequest request,
                                 HttpServletResponse response) {
        if (StringUtils.isNotBlank(suggestInfo.getContent()) &&
                (StringUtils.isNotBlank(suggestInfo.getPhone()) || StringUtils.isNotBlank(suggestInfo.getEmail()))) {
            Long userId = (Long) request.getSession().getAttribute(Constants.SESSIONID);
            suggestInfo.setUserId(userId);
            suggestInfoService.save(suggestInfo, request);
            return HttpServletResponseUtil.processSuccessStatus(response, Constants.OK);
        }
        return HttpServletResponseUtil.processErrorStatus(response, Constants.ERROR);
    }

    /**
     * 保存系统提醒添加记录
     *
     * @param reminder 实体
     * @param request  请求
     * @return 页面
     */
    @RequestMapping(value = "/addRemind", method = RequestMethod.POST)
    @SystemControllerLog(description = "保存系统提醒添加记录")
    @ResponseBody
    public String addRemind(Reminder reminder, HttpServletRequest request,
                            HttpServletResponse response) {
        if (StringUtils.isNotBlank(reminder.getTitle()) && null != reminder.getStart()) {
            Long userId = (Long) request.getSession().getAttribute(Constants.SESSIONID);
            reminder.setUserId(userId);
            reminderService.save(reminder, request);
            return HttpServletResponseUtil.processSuccessStatus(response, Constants.OK);
        }
        return HttpServletResponseUtil.processErrorStatus(response, Constants.ERROR);
    }

    /**
     * 更新系统提醒修改记录
     *
     * @param reminder 实体
     * @param request  请求
     * @return 页面
     */
    @RequestMapping(value = "/editRemind", method = RequestMethod.POST)
    @SystemControllerLog(description = "更新系统提醒修改记录")
    @ResponseBody
    public String editRemind(Reminder reminder, HttpServletRequest request,
                             HttpServletResponse response) {
        if (StringUtils.isNotBlank(reminder.getTitle()) && null != reminder.getStart()) {
            Reminder oldReminder = reminderService.selectByKey(reminder.getId());
            BeanUtils.copyProperties(reminder, oldReminder);
            reminderService.updateAll(oldReminder, request);
            return HttpServletResponseUtil.processSuccessStatus(response, Constants.OK);
        }
        return HttpServletResponseUtil.processErrorStatus(response, Constants.ERROR);
    }

    /**
     * 获取系统提醒列表
     *
     * @return 页面
     */
    @RequestMapping(value = "/getRemindByTime")
    @SystemControllerLog(description = "获取系统提醒列表")
    @ResponseBody
    public String getRemindByTime(Date startTime, Date endTime, HttpServletResponse response) {
        List<Reminder> list = reminderService.findByTime(startTime, endTime);
        AjaxJson aj = new AjaxJson();
        aj.setSuccess(true);
        aj.setObj(list);
        return HttpServletResponseUtil.processSuccessStatus(response, aj);
    }

    /**
     * 删除系统提醒记录
     *
     * @param id      主键
     * @param request 请求
     * @return 页面
     */
    @RequestMapping(value = "/deleteRemind", method = RequestMethod.POST)
    @SystemControllerLog(description = "删除系统提醒记录")
    @ResponseBody
    public String deleteRemind(Long id, HttpServletRequest request, HttpServletResponse response) {
        if (id != null) {
            reminderService.delete(id, request);
            return HttpServletResponseUtil.processSuccessStatus(response, Constants.OK);
        }
        return HttpServletResponseUtil.processErrorStatus(response, Constants.ERROR);
    }

}
