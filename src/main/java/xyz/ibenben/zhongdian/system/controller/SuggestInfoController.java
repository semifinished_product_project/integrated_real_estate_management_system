package xyz.ibenben.zhongdian.system.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import xyz.ibenben.zhongdian.common.annotation.SystemControllerLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.SuggestInfo;
import xyz.ibenben.zhongdian.system.form.SuggestInfoForm;
import xyz.ibenben.zhongdian.system.service.SuggestInfoService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * 意见及建议控制类
 * 提供了一些基本的服务，如初始化、获取意见及建议列表等方法。
 * spring mvc思想 通过controller来调用service里的方法，
 * service里的方法再通过调用Dao来实现对数据的操作
 * 返回值统统是String类型的返回页面有统一的安排方便代码提取
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Slf4j
@Controller
@SessionAttributes("suggestInfoForm")
@RequestMapping("/suggestInfo")
@SuppressWarnings(value = "unchecked")
public class SuggestInfoController extends BaseController {
    /**
     * 实体名称
     */
    private static final String ENTITYNAME = "suggestInfo";

    @Resource
    private SuggestInfoService suggestInfoService;

    /**
     * 初始化
     *
     * @param map             参数
     * @param suggestInfoForm 条件参数
     */
    @ModelAttribute
    public void init(Model map, @ModelAttribute SuggestInfoForm suggestInfoForm) {
        SuggestInfoForm newSuggestInfoForm = new SuggestInfoForm();
        if (suggestInfoForm != null) {
            newSuggestInfoForm = suggestInfoForm;
        }
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), newSuggestInfoForm);
    }

    /**
     * 获取意见及建议列表
     *
     * @param map             参数
     * @param suggestInfoForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/getAll")
    @SystemControllerLog(description = "获取意见及建议列表")
    public String getAll(Model map, @ModelAttribute SuggestInfoForm suggestInfoForm) {
        log.info("getAll param SuggestInfoForm: {}", suggestInfoForm.toString());
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), suggestInfoForm);
        Page<SuggestInfo> page = PageHelper.startPage(suggestInfoForm.getPageIndex(), suggestInfoForm.getPageSize());
        List<SuggestInfo> list = suggestInfoService.findAll(suggestInfoForm);
        map.addAttribute(String.format(Constants.LISTENTITY, ENTITYNAME), list);
        map.addAttribute(Constants.PI, page.toPageInfo());
        return String.format(Constants.LISTPAGE, ENTITYNAME);
    }

    /**
     * 删除意见及建议记录
     *
     * @param id      主键
     * @param request 请求
     * @return 页面
     */
    @RequestMapping(value = "/delete")
    @SystemControllerLog(description = "删除意见及建议记录")
    public String delete(@RequestParam("id") Long id, HttpServletRequest request, SessionStatus status) {
        suggestInfoService.delete(id, request);
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 删除意见及建议列表
     *
     * @param suggestInfoForm 条件参数
     * @param request         请求
     * @return 页面
     */
    @RequestMapping("/deleteAll")
    @SystemControllerLog(description = "删除意见及建议列表")
    public String deleteAll(@ModelAttribute SuggestInfoForm suggestInfoForm,
                            HttpServletRequest request, SessionStatus status) {
        if (suggestInfoForm.getSelectIds() != null) {
            String[] ids = suggestInfoForm.getSelectIds().split(Constants.COMMA);
            for (String id : ids) {
                suggestInfoService.delete(Long.parseLong(id), request);
            }
        }
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 打开意见及建议添加页面
     *
     * @param map 参数
     * @return 页面
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开意见及建议添加页面")
    public String openAddPage(Model map) {
        map.addAttribute(ENTITYNAME, new SuggestInfo());
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 保存意见及建议添加记录
     *
     * @param suggestInfo   实体
     * @param bindingResult 校验
     * @param request       请求
     * @return 页面
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @SystemControllerLog(description = "保存意见及建议添加记录")
    public String postAddMessage(@Valid @ModelAttribute(ENTITYNAME) SuggestInfo suggestInfo,
                                 BindingResult bindingResult, HttpServletRequest request, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            suggestInfoService.save(suggestInfo, request);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        }
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 浏览意见及建议记录
     *
     * @param map             参数
     * @param id              主键
     * @param suggestInfoForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    @SystemControllerLog(description = "浏览意见及建议记录")
    public String openViewPage(Model map, @RequestParam("id") Long id,
                               @ModelAttribute SuggestInfoForm suggestInfoForm) {
        SuggestInfo info = suggestInfoService.findByKey(id);
        map.addAttribute(ENTITYNAME, info);
        return String.format(Constants.VIEWPAGE, ENTITYNAME);
    }

}

