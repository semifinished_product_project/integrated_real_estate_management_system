package xyz.ibenben.zhongdian.system.controller.sys;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import xyz.ibenben.zhongdian.common.annotation.SystemControllerLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.common.shiro.ShiroService;
import xyz.ibenben.zhongdian.system.controller.BaseController;
import xyz.ibenben.zhongdian.system.entity.sys.SysResources;
import xyz.ibenben.zhongdian.system.form.sys.SysResourcesForm;
import xyz.ibenben.zhongdian.system.service.sys.SysResourcesService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * 系统资源控制类
 * 提供了一些基本的服务，如初始化、获取资源列表等方法。
 * spring mvc思想 通过controller来调用service里的方法，
 * service里的方法再通过调用Dao来实现对数据的操作
 * 返回值统统是String类型的返回页面有统一的安排方便代码提取
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Controller
@SessionAttributes("sysResourcesForm")
@RequestMapping("/sys/sysResources")
public class ResourcesController extends BaseController {
    /**
     * 实体名称
     */
    private static final String ENTITYNAME = "sysResources";

    @Resource
    private SysResourcesService resourcesService;
    @Resource
    private ShiroService shiroService;

    /**
     * 初始化
     *
     * @param map              参数
     * @param sysResourcesForm 条件参数
     */
    @ModelAttribute
    public void init(Model map, @ModelAttribute SysResourcesForm sysResourcesForm) {
        SysResourcesForm newSysResourcesForm = new SysResourcesForm();
        if (sysResourcesForm != null) {
            newSysResourcesForm = sysResourcesForm;
        }
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), newSysResourcesForm);
    }

    /**
     * 获取资源列表
     *
     * @param map              参数
     * @param sysResourcesForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/getAll")
    @SystemControllerLog(description = "获取资源列表")
    public String getAll(Model map, @ModelAttribute SysResourcesForm sysResourcesForm) {
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), sysResourcesForm);
        Page<SysResources> page = PageHelper.startPage(sysResourcesForm.getPageIndex(), sysResourcesForm.getPageSize());
        List<SysResources> list = resourcesService.findAll(sysResourcesForm);
        map.addAttribute(String.format(Constants.LISTENTITY, ENTITYNAME), list);
        map.addAttribute(Constants.PI, page.toPageInfo());
        return "sys/" + String.format(Constants.LISTPAGE, ENTITYNAME);
    }

    /**
     * 删除资源记录
     *
     * @param id      主键
     * @param request 请求
     * @return 页面
     */
    @RequestMapping(value = "/delete")
    @SystemControllerLog(description = "删除资源记录")
    public String delete(Long id, HttpServletRequest request, SessionStatus status) {
        resourcesService.delete(id, request);
        //更新权限
        shiroService.updatePermission();
        status.setComplete();
        return String.format(Constants.REDIRECT, "sys/" + ENTITYNAME);
    }

    /**
     * 删除资源列表
     *
     * @param sysResourcesForm 条件参数
     * @param request          请求
     * @return 页面
     */
    @RequestMapping("/deleteAll")
    @SystemControllerLog(description = "删除资源列表")
    public String deleteAll(@ModelAttribute SysResourcesForm sysResourcesForm, HttpServletRequest request, SessionStatus status) {
        if (sysResourcesForm.getSelectIds() != null) {
            String[] ids = sysResourcesForm.getSelectIds().split(Constants.COMMA);
            for (String id : ids) {
                resourcesService.delete(Long.parseLong(id), request);
            }
        }
        status.setComplete();
        return String.format(Constants.REDIRECT, "sys/" + ENTITYNAME);
    }

    /**
     * 打开资源添加页面
     *
     * @param map 参数
     * @return 页面
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开资源添加页面")
    public String openAddPage(Model map) {
        map.addAttribute(ENTITYNAME, new SysResources());
        return "sys/" + String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 保存资源添加记录
     *
     * @param sysResources  实体
     * @param bindingResult 校验
     * @param request       请求
     * @return 页面
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @SystemControllerLog(description = "保存资源添加记录")
    public String postAddMessage(@Valid @ModelAttribute(ENTITYNAME) SysResources sysResources,
                                 BindingResult bindingResult, HttpServletRequest request, SessionStatus status) {
        SysResources resource = resourcesService.selectByName(sysResources.getName());
        if (null != resource) {
            bindingResult.rejectValue("name", "misFormat", "这个资源名称已经存在了！请更改!");
            return "sys/" + String.format(Constants.ADDPAGE, ENTITYNAME);
        }
        if (!bindingResult.hasErrors()) {
            resourcesService.save(sysResources, request);
            //更新权限
            shiroService.updatePermission();
            status.setComplete();
            return String.format(Constants.REDIRECT, "sys/" + ENTITYNAME);
        } else {
            return "sys/" + String.format(Constants.ADDPAGE, ENTITYNAME);
        }
    }

    /**
     * 浏览资源记录
     *
     * @param map              参数
     * @param id               主键
     * @param sysResourcesForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    @SystemControllerLog(description = "浏览资源记录")
    public String openViewPage(Model map, Long id, @ModelAttribute SysResourcesForm sysResourcesForm) {
        map.addAttribute(ENTITYNAME, resourcesService.selectByKey(id));
        return "sys/" + String.format(Constants.VIEWPAGE, ENTITYNAME);
    }

}
