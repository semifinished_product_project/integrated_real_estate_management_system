package xyz.ibenben.zhongdian.system.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import xyz.ibenben.zhongdian.common.annotation.SystemControllerLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.DiscountInfo;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;
import xyz.ibenben.zhongdian.system.form.DiscountInfoForm;
import xyz.ibenben.zhongdian.system.service.CompanyInfoService;
import xyz.ibenben.zhongdian.system.service.DiscountInfoService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

/**
 * 公司信息控制类
 * 提供了一些基本的服务，如初始化、获取公司信息列表等方法。
 * spring mvc思想 通过controller来调用service里的方法，
 * service里的方法再通过调用Dao来实现对数据的操作
 * 返回值统统是String类型的返回页面有统一的安排方便代码提取
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Slf4j
@Controller
@SessionAttributes("discountInfoForm")
@RequestMapping("/discountInfo")
@SuppressWarnings(value = "unchecked")
public class DiscountInfoController extends BaseController {
    /**
     * 实体名称
     */
    private static final String ENTITYNAME = "discountInfo";

    @Resource
    private DiscountInfoService discountInfoService;
    @Resource
    private CompanyInfoService companyInfoService;

    /**
     * 初始化
     *
     * @param map              参数
     * @param discountInfoForm 条件参数
     */
    @ModelAttribute
    public void init(Model map, @ModelAttribute DiscountInfoForm discountInfoForm, HttpSession session) {
        map.addAttribute("companyInfoList", companyInfoService.selectAll());
        DiscountInfoForm newDiscountInfoForm = new DiscountInfoForm();
        if (discountInfoForm != null) {
            newDiscountInfoForm = discountInfoForm;
        }
        SysUser user = (SysUser) session.getAttribute(Constants.SESSION);
        if (user != null && user.getSysRole() != null) {
            newDiscountInfoForm.setCompanyId(user.getSysRole().getCompanyId());
        }
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), newDiscountInfoForm);
    }

    /**
     * 获取折扣信息列表
     *
     * @param map              参数
     * @param discountInfoForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/getAll")
    @SystemControllerLog(description = "获取折扣信息列表")
    public String getAll(Model map, @ModelAttribute DiscountInfoForm discountInfoForm) {
        log.info("getAll param DiscountInfoForm: {}", discountInfoForm.toString());
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), discountInfoForm);
        Page<DiscountInfo> page = PageHelper.startPage(discountInfoForm.getPageIndex(), discountInfoForm.getPageSize());
        List<DiscountInfo> list = discountInfoService.findAll(discountInfoForm);
        map.addAttribute(String.format(Constants.LISTENTITY, ENTITYNAME), list);
        map.addAttribute(Constants.PI, page.toPageInfo());
        return String.format(Constants.LISTPAGE, ENTITYNAME);
    }

    /**
     * 删除折扣信息记录
     *
     * @param id      主键
     * @param request 请求
     * @return 页面
     */
    @RequestMapping(value = "/delete")
    @SystemControllerLog(description = "删除折扣信息记录")
    public String delete(@RequestParam("id") Long id, HttpServletRequest request, SessionStatus status) {
        discountInfoService.delete(id, request);
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 删除折扣信息列表
     *
     * @param discountInfoForm 条件参数
     * @param request          请求
     * @return 页面
     */
    @RequestMapping("/deleteAll")
    @SystemControllerLog(description = "删除折扣信息列表")
    public String deleteAll(@ModelAttribute DiscountInfoForm discountInfoForm,
                            HttpServletRequest request, SessionStatus status) {
        if (discountInfoForm.getSelectIds() != null) {
            String[] ids = discountInfoForm.getSelectIds().split(Constants.COMMA);
            for (String id : ids) {
                discountInfoService.delete(Long.parseLong(id), request);
            }
        }
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 打开折扣信息添加页面
     *
     * @param map 参数
     * @return 页面
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开折扣信息添加页面")
    public String openAddPage(Model map) {
        map.addAttribute(ENTITYNAME, new DiscountInfo());
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 保存折扣信息添加记录
     *
     * @param discountInfo  实体
     * @param bindingResult 校验
     * @param request       请求
     * @return 页面
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @SystemControllerLog(description = "保存折扣信息添加记录")
    public String postAddMessage(@Valid @ModelAttribute(ENTITYNAME) DiscountInfo discountInfo,
                                 BindingResult bindingResult, HttpServletRequest request, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            discountInfoService.save(discountInfo, request);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        } else {
            return String.format(Constants.ADDPAGE, ENTITYNAME);
        }
    }

    /**
     * 打开折扣信息修改页面
     *
     * @param map 参数
     * @param id  主键
     * @return 页面
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    @SystemControllerLog(description = "打开折扣信息修改页面")
    public String openEditPage(Model map, @RequestParam("id") Long id) {
        map.addAttribute(ENTITYNAME, discountInfoService.selectByKey(id));
        return String.format(Constants.EDITPAGE, ENTITYNAME);
    }

    /**
     * 更新折扣信息修改记录
     *
     * @param discountInfo  实体
     * @param bindingResult 校验
     * @param request       请求
     * @return 页面
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @SystemControllerLog(description = "更新折扣信息修改记录")
    public String postEditMessage(@Valid @ModelAttribute(ENTITYNAME) DiscountInfo discountInfo,
                                  BindingResult bindingResult, HttpServletRequest request, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            discountInfoService.updateNotNull(discountInfo, request);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        } else {
            return String.format(Constants.EDITPAGE, ENTITYNAME);
        }
    }

}

