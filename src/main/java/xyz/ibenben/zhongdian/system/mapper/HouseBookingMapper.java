package xyz.ibenben.zhongdian.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.HouseBooking;
import xyz.ibenben.zhongdian.system.form.HouseBookingForm;

import java.util.List;

/**
 * 房屋预售Mapper类
 * 提供了一些基本的服务。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface HouseBookingMapper extends BaseMapper<HouseBooking> {
    /**
     * 根据条件查询列表
     *
     * @param houseBookingForm 条件
     * @return 列表
     */
    List<HouseBooking> findAll(HouseBookingForm houseBookingForm);
}