package xyz.ibenben.zhongdian.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.StuffInfo;
import xyz.ibenben.zhongdian.system.form.StuffInfoForm;

import java.util.List;

/**
 * 房屋物品信息Mapper类
 * 提供了一些基本的服务，如获取所有房屋物品信息等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface StuffInfoMapper extends BaseMapper<StuffInfo> {
    /**
     * 根据条件查询列表
     *
     * @param stuffInfoForm 条件
     * @return 列表
     */
    List<StuffInfo> findAll(StuffInfoForm stuffInfoForm);

    /**
     * 根据主键获取记录
     *
     * @param id 主键
     * @return 记录
     */
    StuffInfo findByKey(@Param("id") Long id);
}