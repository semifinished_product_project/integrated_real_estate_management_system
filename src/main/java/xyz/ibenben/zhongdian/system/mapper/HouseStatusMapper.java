package xyz.ibenben.zhongdian.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.HouseStatus;
import xyz.ibenben.zhongdian.system.form.HouseStatusForm;

import java.util.List;

/**
 * 房屋状态Mapper类
 * 提供了一些基本的服务。
 * 属于产权下的一个分支
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface HouseStatusMapper extends BaseMapper<HouseStatus> {
    /**
     * 根据条件查询列表
     *
     * @param houseStatusForm 条件
     * @return 列表
     */
    List<HouseStatus> findAll(HouseStatusForm houseStatusForm);
}