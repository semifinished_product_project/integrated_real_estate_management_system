package xyz.ibenben.zhongdian.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.InnerMail;
import xyz.ibenben.zhongdian.system.form.InnerMailForm;

import java.util.List;
import java.util.Map;

/**
 * 站内信Mapper类
 * 提供了一些基本的服务，如翻页查询、根据用户主键查找前台公告、根据主键获取记录带列表等方法
 * 主要用于用户之间通信使用此类来维护
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface InnerMailMapper extends BaseMapper<InnerMail> {
    /**
     * 翻页查询
     *
     * @param map 参数
     * @return 返回值
     */
    List<InnerMail> getAll(Map<String, Object> map);

    /**
     * 根据用户主键查找前台公告
     *
     * @param map 参数
     * @return 返回值
     */
    List<InnerMail> findAllNoticeByUserId(Map<String, Object> map);

    /**
     * 根据主键获取记录带列表
     *
     * @param map 参数
     * @return 返回值
     */
    InnerMail findByPrimaryKey(Map<String, Object> map);

    /**
     * 根据条件查询列表
     *
     * @param innerMailForm 条件
     * @return 列表
     */
    List<InnerMail> findAll(InnerMailForm innerMailForm);

    /**
     * 根据条件查询列表数量
     *
     * @param map 参数
     * @return 数量
     */
    int findAllNoticeCount(Map<String, Object> map);
}