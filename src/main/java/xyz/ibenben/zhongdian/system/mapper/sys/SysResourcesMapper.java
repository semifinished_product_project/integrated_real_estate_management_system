package xyz.ibenben.zhongdian.system.mapper.sys;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.sys.SysResource;
import xyz.ibenben.zhongdian.system.entity.sys.SysResources;
import xyz.ibenben.zhongdian.system.form.sys.SysResourcesForm;

import java.util.List;

/**
 * 系统资源Mapper类
 * 系统级资源管理所使用的，是权限系统中的一部分
 * 提供了一些基本的服务，如获取用户资源、查询资源列表等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface SysResourcesMapper extends BaseMapper<SysResources> {
    /**
     * 读取用户资源信息
     *
     * @param userId 用户主键
     * @return 返回值
     */
    List<SysResources> loadUserResources(@Param("userId") Long userId);

    /**
     * 查询资源列表
     *
     * @param rid 参数
     * @return 返回值
     */
    List<SysResource> queryResourcesListWithSelected(@Param("rid") Long rid);

    /**
     * 根据类型查询列表
     *
     * @param type 类型
     * @return 返回值
     */
    List<SysResources> findByType(@Param("type") Integer type);

    /**
     * 根据条件查询列表
     *
     * @param sysResourcesForm 条件
     * @return 列表
     */
    List<SysResources> findAll(SysResourcesForm sysResourcesForm);
}