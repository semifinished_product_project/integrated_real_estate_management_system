package xyz.ibenben.zhongdian.system.mapper.sys;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.sys.SysRole;
import xyz.ibenben.zhongdian.system.form.sys.SysRoleForm;

import java.util.List;

/**
 * 系统角色Mapper类
 * 系统级角色管理所使用的，是权限系统中的一部分
 * 提供了一些基本的服务，如查询角色列表等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {
    /**
     * 查询角色列表
     *
     * @param uid 用户主键
     * @return 角色列表
     */
    List<SysRole> queryRoleListWithSelected(@Param("uid") Long uid);

    /**
     * 根据角色描述查询角色列表
     *
     * @param roleDesc 角色描述
     * @return 角色列表
     */
    List<SysRole> queryRoleListWithSelectedByRole(@Param("roleDesc") String roleDesc);

    /**
     * 根据角色主键查找对应信息
     *
     * @param id 角色主键
     * @return 角色信息
     */
    SysRole selectRoleWithCompanyById(@Param("id") Long id);

    /**
     * 根据条件查找列表
     *
     * @param sysRoleForm 条件
     * @return 列表
     */
    List<SysRole> findAll(SysRoleForm sysRoleForm);
}