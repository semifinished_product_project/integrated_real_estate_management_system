package xyz.ibenben.zhongdian.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.DealRecordInfo;
import xyz.ibenben.zhongdian.system.form.DealRecordInfoForm;

import java.util.List;

/**
 * 成交记录信息Mapper类
 * 提供了一些基本的服务，如获取所有成交记录信息等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface DealRecordInfoMapper extends BaseMapper<DealRecordInfo> {
    /**
     * 根据条件查询列表
     *
     * @param dealRecordInfoForm 条件
     * @return 列表
     */
    List<DealRecordInfo> findAll(DealRecordInfoForm dealRecordInfoForm);

    /**
     * 根据主键获取记录
     *
     * @param id 主键
     * @return 记录
     */
    DealRecordInfo findByKey(@Param("id") Long id);
}