package xyz.ibenben.zhongdian.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;
import xyz.ibenben.zhongdian.system.entity.ChatRecord;
import xyz.ibenben.zhongdian.system.form.ChatRecordForm;

import java.util.List;
import java.util.Map;

/**
 * 聊天记录Mapper类
 * 提供了一些基本的服务，如获取指定数量的记录列表等方法。
 * 是用户在互相聊天时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface ChatRecordMapper extends BaseMapper<ChatRecord> {
    /**
     * 获取指定数量的记录列表
     *
     * @param map 参数
     * @return 聊天记录列表
     */
    List<ChatRecord> selectByLimit(Map<String, Object> map);

    /**
     * 获取指定类型的记录数量
     *
     * @param map 参数
     * @return 聊天记录数量
     */
    int selectByCount(Map<String, Object> map);

    /**
     * 根据条件查询列表
     *
     * @param chatRecordForm 条件
     * @return 列表
     */
    List<ChatRecord> findAll(ChatRecordForm chatRecordForm);

    /**
     * 根据主键获取记录
     *
     * @param id 主键
     * @return 记录
     */
    ChatRecord findByKey(@Param("id") Long id);
}