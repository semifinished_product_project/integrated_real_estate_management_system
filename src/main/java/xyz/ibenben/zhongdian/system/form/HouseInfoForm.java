package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.HouseTypeEnum;
import xyz.ibenben.zhongdian.system.entity.enums.OrientationEnum;

import java.math.BigDecimal;

/**
 * 房屋信息查询条件类
 * 提供了对房屋名称，室数量类型等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
@ToString
public class HouseInfoForm extends SearchForm {
    private static final long serialVersionUID = 6121524867379737233L;

    /**
     * 房屋名称
     */
    protected String houseName;

    /**
     * 室数量
     */
    protected Integer roomNumber;

    /**
     * 厅数量
     */
    protected Integer drawingNumber;

    /**
     * 厨数量
     */
    protected Integer kitchenNumber;

    /**
     * 卫数量
     */
    protected Integer restroomNumber;

    /**
     * 朝向
     */
    protected OrientationEnum orientation;

    /**
     * 建筑面积
     */
    protected BigDecimal houseArea;

    /**
     * 所在楼层
     */
    protected Integer layer;

    /**
     * 小区
     */
    protected String polt;

    /**
     * 房屋类型
     */
    protected HouseTypeEnum type;

    /**
     * 省
     */
    protected Long province;

    /**
     * 市
     */
    protected Long city;

    /**
     * 区
     */
    protected Long region;

}
