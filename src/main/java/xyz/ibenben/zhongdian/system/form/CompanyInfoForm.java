package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.CompanyTypeEnum;

/**
 * 公司信息查询条件类
 * 提供了对公司名称，公司类型等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
@ToString
public class CompanyInfoForm extends SearchForm {
    private static final long serialVersionUID = 6121524867379737233L;

    /**
     * 公司名称
     */
    protected String companyName;

    /**
     * 联系电话
     */
    protected String phone;

    /**
     * 公司类型
     */
    protected CompanyTypeEnum type;

    /**
     * 省
     */
    protected Long province;

    /**
     * 市
     */
    protected Long city;

    /**
     * 区
     */
    protected Long region;

}
