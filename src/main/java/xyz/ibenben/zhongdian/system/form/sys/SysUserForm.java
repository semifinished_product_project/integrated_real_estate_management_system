package xyz.ibenben.zhongdian.system.form.sys;

import lombok.Getter;
import lombok.Setter;
import xyz.ibenben.zhongdian.system.form.SearchForm;

/**
 * 系统用户查询条件类
 * 提供了对主键,用户名等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class SysUserForm extends SearchForm {
    private static final long serialVersionUID = 417168289368042148L;

    /**
     * 主键
     */
    protected Long id;

    /**
     * 用户名
     */
    protected String username;

    /**
     * 是否开启
     */
    protected Integer enable;

    /**
     * 类型
     */
    protected Boolean type;

}
