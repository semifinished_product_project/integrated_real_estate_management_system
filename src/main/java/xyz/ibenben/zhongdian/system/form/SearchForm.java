package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 查询条件抽象类
 * 此类用于根据用户所输入的条件查询结果显示到列表里
 * 或者是使用全选或全不选来删除对应记录是用到的抽象类
 * selectIds代表所对应的selectbox
 * pageSize代表每页条目数
 * pageIndex代表页码数
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class SearchForm implements Serializable {
    private static final long serialVersionUID = -4075201800269162352L;
    /**
     * 是否选中
     */
    protected String selectIds;

    /**
     * 每页条目数
     */
    protected int pageSize = 10;

    /**
     * 页码
     */
    protected int pageIndex = 1;

    /**
     * 公司主键
     */
    protected Long companyId;

}
