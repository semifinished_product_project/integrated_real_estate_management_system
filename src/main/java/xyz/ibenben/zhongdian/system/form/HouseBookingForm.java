package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 房屋预售查询条件类
 * 提供了对名称,省份等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class HouseBookingForm extends SearchForm {
    private static final long serialVersionUID = 9061481497845474694L;

    /**
     * 名称
     */
    protected String name;

    /**
     * 省份
     */
    protected Long province;

    /**
     * 市区
     */
    protected Long city;

    /**
     * 区县
     */
    protected Long region;

    /**
     * 房屋面积
     */
    protected BigDecimal houseArea;

}
