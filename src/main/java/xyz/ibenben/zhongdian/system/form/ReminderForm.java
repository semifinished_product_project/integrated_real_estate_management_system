package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 系统提醒查询条件类
 * 提供了对内容,需要提醒的时间等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class ReminderForm extends SearchForm {
    private static final long serialVersionUID = -2465644184106576265L;

    /**
     * 用户主键
     */
    private Long userId;

    /**
     * 用户名称
     */
    private String username;

    /**
     * 需要提醒的内容
     */
    private String title;

    /**
     * 需要提醒的时间
     */
    private Date remindTime;

}
