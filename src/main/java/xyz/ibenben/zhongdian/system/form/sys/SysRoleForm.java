package xyz.ibenben.zhongdian.system.form.sys;

import lombok.Getter;
import lombok.Setter;
import xyz.ibenben.zhongdian.system.form.SearchForm;

/**
 * 系统角色查询条件类
 * 提供了对角色描述等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class SysRoleForm extends SearchForm {
    private static final long serialVersionUID = 8020720725619793592L;

    /**
     * 角色描述
     */
    protected String roleDesc;

}
