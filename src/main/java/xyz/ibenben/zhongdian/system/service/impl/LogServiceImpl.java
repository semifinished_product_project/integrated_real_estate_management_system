package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.system.dao.LogDao;
import xyz.ibenben.zhongdian.system.entity.Log;
import xyz.ibenben.zhongdian.system.mapper.LogMapper;
import xyz.ibenben.zhongdian.system.service.LogService;

import javax.annotation.Resource;

/**
 * 日志服务实现类
 * 提供了一些基本的服务
 * 记录日志到数据库
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class LogServiceImpl extends AbstractServiceImpl<Log> implements LogService {
    @Resource
    private LogDao logDao;
    @Resource
    private LogMapper logMapper;

}
