package xyz.ibenben.zhongdian.system.service.elasticsearch;

import java.util.List;

/**
 * @author chenjian
 */
public interface ElasticsearchService<T> {
    /**
     * 根据主键获取对象
     *
     * @param key 参数
     * @return 返回值
     */
    T selectByKey(Object key);

    /**
     * 保存对象
     *
     * @param type   表
     * @param entity 参数
     * @return 返回值
     */
    void save(String type, T entity);

    /**
     * 删除对象
     *
     * @param type 表
     * @param key  参数
     */
    void delete(String type, Object key);

    /**
     * 更新所有对象
     *
     * @param type   表
     * @param entity 参数
     * @return 返回值
     */
    void updateAll(String type, T entity);

    /**
     * 获取所有对象
     *
     * @return 返回值
     */
    List<T> selectAll();

}
