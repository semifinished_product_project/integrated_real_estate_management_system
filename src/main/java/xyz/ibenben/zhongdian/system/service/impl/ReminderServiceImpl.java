package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.system.dao.ReminderDao;
import xyz.ibenben.zhongdian.system.entity.Reminder;
import xyz.ibenben.zhongdian.system.form.ReminderForm;
import xyz.ibenben.zhongdian.system.mapper.ReminderMapper;
import xyz.ibenben.zhongdian.system.service.ReminderService;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 系统提醒服务实现类
 * 提供了一些基本的服务，如获根据条件查询列表等方法。
 * 是用户在系统提醒时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class ReminderServiceImpl extends AbstractServiceImpl<Reminder> implements ReminderService {
    @Resource
    private ReminderDao reminderDao;
    @Resource
    private ReminderMapper reminderMapper;

    @Override
    public List<Reminder> findAll(ReminderForm reminderForm) {
        return reminderMapper.findAll(reminderForm);
    }

    @Override
    public Reminder findByKey(Long id) {
        return reminderMapper.findByKey(id);
    }

    @Override
    public List<Reminder> findByTime(Date startTime, Date endTime) {
        return reminderMapper.findByTime(startTime, endTime);
    }
}
