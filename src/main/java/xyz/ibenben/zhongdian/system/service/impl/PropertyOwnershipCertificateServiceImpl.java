package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.system.dao.PropertyOwnershipCertificateDao;
import xyz.ibenben.zhongdian.system.entity.PropertyOwnershipCertificate;
import xyz.ibenben.zhongdian.system.form.PropertyOwnershipCertificateForm;
import xyz.ibenben.zhongdian.system.mapper.PropertyOwnershipCertificateMapper;
import xyz.ibenben.zhongdian.system.service.PropertyOwnershipCertificateService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 产权服务实现类
 * 提供了一些基本的服务
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class PropertyOwnershipCertificateServiceImpl extends AbstractServiceImpl<PropertyOwnershipCertificate>
        implements PropertyOwnershipCertificateService {

    @Resource
    private PropertyOwnershipCertificateDao propertyOwnershipCertificateDao;
    @Resource
    private PropertyOwnershipCertificateMapper propertyOwnershipCertificateMapper;

    @Override
    public List<PropertyOwnershipCertificate> findAll(PropertyOwnershipCertificateForm propertyOwnershipCertificateForm) {
        return propertyOwnershipCertificateMapper.findAll(propertyOwnershipCertificateForm);
    }
}
