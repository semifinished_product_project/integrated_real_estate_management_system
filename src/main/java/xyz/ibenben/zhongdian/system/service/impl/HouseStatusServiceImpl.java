package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.dao.HouseStatusDao;
import xyz.ibenben.zhongdian.system.entity.HouseStatus;
import xyz.ibenben.zhongdian.system.form.HouseStatusForm;
import xyz.ibenben.zhongdian.system.mapper.HouseStatusMapper;
import xyz.ibenben.zhongdian.system.service.HouseStatusService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 房屋状态服务实现类
 * 提供了一些基本的服务，如根据房产证查找房屋状态记录列表等方法。
 * 属于产权下的一个分支
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class HouseStatusServiceImpl extends AbstractServiceImpl<HouseStatus> implements HouseStatusService {
    @Resource
    private HouseStatusDao houseStatusDao;
    @Resource
    private HouseStatusMapper houseStatusMapper;

    /**
     * findByOwnerId房屋状态
     *
     * @param id 主键
     * @return 房屋状态列表
     */
    @Override
    @SystemServiceLog(description = "findByOwnerId房屋状态")
    public List<HouseStatus> findByOwnerId(Long id) {
        //根据拥有者主键查找房屋状态列表
        Example example = new Example(HouseStatus.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("ownerId", id);
        return houseStatusDao.selectByExample(example);
    }

    /**
     * 根据拥有者主键删除记录
     *
     * @param id      拥有者主键
     * @param request 请求
     */
    @Override
    @SystemServiceLog(description = "deleteByOwnerId房屋状态")
    public void deleteByOwnerId(Long id, HttpServletRequest request) {
        List<HouseStatus> list = this.findByOwnerId(id);
        for (HouseStatus houseStatus : list) {
            this.delete(houseStatus.getId(), request);
        }
    }

    @Override
    public List<HouseStatus> findAll(HouseStatusForm houseStatusForm) {
        return houseStatusMapper.findAll(houseStatusForm);
    }
}
