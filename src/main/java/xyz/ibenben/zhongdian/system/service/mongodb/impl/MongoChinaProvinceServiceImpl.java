package xyz.ibenben.zhongdian.system.service.mongodb.impl;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.entity.mongodb.MongoChinaProvince;
import xyz.ibenben.zhongdian.system.service.mongodb.MongoChinaProvinceService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 省市服务实现类
 * 此类提供对省市查询的服务，是三级联动里的第一级
 * 提供了一些基本的服务，如通过代码查列表等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class MongoChinaProvinceServiceImpl extends AbstractMongoServiceImpl<MongoChinaProvince> implements MongoChinaProvinceService {
    @Resource
    private MongoTemplate mongoTemplate;

    /**
     * findNameByCode省份记录
     *
     * @param provinceCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findNameByCode省份记录")
    public String findNameByCode(Long provinceCode) {
        String resultStr = "";
        //返回结果
        if (null != provinceCode) {
            //组装实体
            Query query = new Query(Criteria.where("provinceCode").is(provinceCode));
            MongoChinaProvince result = mongoTemplate.findOne(query, MongoChinaProvince.class);
            if (result != null) {
                resultStr = result.getProvinceName();
            }
        }
        return resultStr;
    }

    /**
     * 通过名称找代码
     *
     * @param provinceName 名称
     * @return 返回值
     */
    @Override
    public List<Long> findCodeByName(String provinceName) {
        List<Long> list = new ArrayList<>();
        //返回结果
        if (null != provinceName && !"".equals(provinceName)) {
            //组装实体
            Query query = new Query(Criteria.where("provinceName").is(provinceName));
            List<MongoChinaProvince> result = mongoTemplate.find(query, MongoChinaProvince.class);
            if (result != null) {
                for (MongoChinaProvince chinaProvince : result) {
                    list.add(chinaProvince.getProvinceCode());
                }
            }
        }
        return list;
    }
}
