package xyz.ibenben.zhongdian.system.service;

import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 通用接口
 * 提供了一些基本的服务, 如根据主键获取对象、保存对象、删除对象等服务
 * 其他服务类只要继承这个类就可以省去很多重复代码，非常适合后台程序的使用
 * 接口使用了泛型技术来实现的功能
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public interface IService<T> {

    /**
     * 根据主键获取对象
     *
     * @param key 参数
     * @return 返回值
     */
    T selectByKey(Object key);

    /**
     * 保存对象
     *
     * @param entity  参数
     * @param request 参数
     * @return 返回值
     */
    int save(T entity, HttpServletRequest request);

    /**
     * 删除对象
     *
     * @param key     参数
     * @param request 参数
     * @return 返回值
     */
    int delete(Object key, HttpServletRequest request);

    /**
     * 更新所有对象
     *
     * @param entity  参数
     * @param request 参数
     * @return 返回值
     */
    int updateAll(T entity, HttpServletRequest request);

    /**
     * 更新所有非空对象
     *
     * @param entity  参数
     * @param request 参数
     * @return 返回值
     */
    int updateNotNull(T entity, HttpServletRequest request);

    /**
     * 获取所有对象
     *
     * @return 返回值
     */
    List<T> selectAll();

    /**
     * 根据实例获取符合实例的对象
     *
     * @param example 参数
     * @return 返回值
     */
    List<T> selectByExample(Example example);
}
