package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.dao.OtherRightDigestDao;
import xyz.ibenben.zhongdian.system.entity.OtherRightDigest;
import xyz.ibenben.zhongdian.system.form.OtherRightDigestForm;
import xyz.ibenben.zhongdian.system.mapper.OtherRightDigestMapper;
import xyz.ibenben.zhongdian.system.service.OtherRightDigestService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 他项权利摘要服务实现类
 * 提供了一些基本的服务，如根据房产证查找他项权利摘要记录列表等方法。
 * 属于产权下的一个分支
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class OtherRightDigestServiceImpl extends AbstractServiceImpl<OtherRightDigest> implements OtherRightDigestService {
    @Resource
    private OtherRightDigestDao otherRightDigestDao;
    @Resource
    private OtherRightDigestMapper otherRightDigestMapper;

    /**
     * findByOwnerId设定他项权利摘要记录
     *
     * @param id 主键
     * @return 他项权利摘要列表
     */
    @Override
    @SystemServiceLog(description = "findByOwnerId设定他项权利摘要记录")
    public List<OtherRightDigest> findByOwnerId(Long id) {
        //根据拥有者主键获取他项权利摘要列表
        Example example = new Example(OtherRightDigest.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("ownerId", id);
        return otherRightDigestDao.selectByExample(example);
    }

    /**
     * 根据拥有者主键删除记录
     *
     * @param id      拥有者主键
     * @param request 请求
     */
    @Override
    public void deleteByOwnerId(Long id, HttpServletRequest request) {
        List<OtherRightDigest> list = this.findByOwnerId(id);
        for (OtherRightDigest otherRightDigest : list) {
            this.delete(otherRightDigest.getId(), request);
        }
    }

    @Override
    public List<OtherRightDigest> findAll(OtherRightDigestForm otherRightDigestForm) {
        return otherRightDigestMapper.findAll(otherRightDigestForm);
    }
}
