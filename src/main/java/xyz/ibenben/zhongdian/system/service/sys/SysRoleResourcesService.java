package xyz.ibenben.zhongdian.system.service.sys;

import xyz.ibenben.zhongdian.system.entity.sys.SysRoleResources;

/**
 * 系统角色资源服务类
 * 系统级角色管理所使用的，是权限系统中的一部分
 * 提供了一些基本的服务，如添加角色资源、通过角色主键删除记录等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface SysRoleResourcesService {
    /**
     * 添加角色资源
     *
     * @param roleResources 参数
     */
    void addRoleResources(SysRoleResources roleResources);

    /**
     * 通过角色主键删除记录
     *
     * @param key 参数
     * @return 返回值
     */
    int deleteByRoleId(Object key);
}
