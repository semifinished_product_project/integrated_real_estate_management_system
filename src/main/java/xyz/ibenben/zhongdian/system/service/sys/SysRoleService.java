package xyz.ibenben.zhongdian.system.service.sys;

import xyz.ibenben.zhongdian.system.entity.sys.SysRole;
import xyz.ibenben.zhongdian.system.form.sys.SysRoleForm;
import xyz.ibenben.zhongdian.system.service.IService;

import java.util.List;

/**
 * 系统角色服务类
 * 系统级角色管理所使用的，是权限系统中的一部分
 * 提供了一些基本的服务，如查询角色列表、根据角色描述查找是否重复等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface SysRoleService extends IService<SysRole> {
    /**
     * 查询角色列表
     *
     * @param uid 参数
     * @return 返回值
     */
    List<SysRole> queryRoleListWithSelected(Long uid);

    /**
     * 根据角色描述查找是否重复
     *
     * @param companyId 公司主键
     * @param roledesc  参数
     * @return 返回值
     */
    SysRole selectByRoleDesc(Long companyId, String roledesc);

    /**
     * 根据角色主键查找对应信息
     *
     * @param id 角色主键
     * @return 角色信息
     */
    SysRole selectRoleWithCompanyById(Long id);

    /**
     * 根据条件查找列表
     *
     * @param sysRoleForm 条件
     * @return 列表
     */
    List<SysRole> findAll(SysRoleForm sysRoleForm);
}
