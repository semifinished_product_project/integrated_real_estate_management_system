package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.dao.ChinaCityDao;
import xyz.ibenben.zhongdian.system.entity.ChinaCity;
import xyz.ibenben.zhongdian.system.service.ChinaCityService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 城市服务实现类
 * 此类提供对城市查询的服务，是三级联动里的第二级
 * 提供了一些基本的服务，如通过代码查列表、通过代码查名称等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class ChinaCityServiceImpl extends AbstractServiceImpl<ChinaCity> implements ChinaCityService {
    @Resource
    private ChinaCityDao chinaCityDao;

    /**
     * findListByProvinceCode城市记录
     *
     * @param provinceCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findListByProvinceCode城市记录")
    public List<ChinaCity> findListByProvinceCode(Long provinceCode) {
        Example example = new Example(ChinaCity.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("provinceCode", provinceCode);
        return chinaCityDao.selectByExample(example);
    }

    /**
     * findNameByCode城市记录
     *
     * @param cityCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findNameByCode城市记录")
    public String findNameByCode(Long cityCode) {
        String resultStr = "";
        if (null != cityCode) {
            ChinaCity city = new ChinaCity();
            city.setCityCode(cityCode);
            ChinaCity result = chinaCityDao.selectOne(city);
            resultStr = result.getCityName();
        }
        return resultStr;
    }

}
