package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.dao.ChinaRegionDao;
import xyz.ibenben.zhongdian.system.entity.ChinaRegion;
import xyz.ibenben.zhongdian.system.service.ChinaRegionService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 区县服务实现类
 * 此类提供对区县查询的服务，是三级联动里的最后一级
 * 提供了一些基本的服务，如通过代码查列表、通过代码查名称等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class ChinaRegionServiceImpl extends AbstractServiceImpl<ChinaRegion> implements ChinaRegionService {
    @Resource
    private ChinaRegionDao chinaRegionDao;

    /**
     * findListByCityCode区域记录
     *
     * @param cityCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findListByCityCode区域记录")
    public List<ChinaRegion> findListByCityCode(Long cityCode) {
        Example example = new Example(ChinaRegion.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("cityCode", cityCode);
        return chinaRegionDao.selectByExample(example);
    }

    /**
     * findNameByCode区域记录
     *
     * @param regionCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findNameByCode区域记录")
    public String findNameByCode(Long regionCode) {
        String resultStr = "";
        if (null != regionCode) {
            ChinaRegion region = new ChinaRegion();
            region.setRegionCode(regionCode);
            ChinaRegion result = chinaRegionDao.selectOne(region);
            resultStr = result.getRegionName();
        }
        return resultStr;
    }

}
