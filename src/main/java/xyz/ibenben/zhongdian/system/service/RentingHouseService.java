package xyz.ibenben.zhongdian.system.service;

import xyz.ibenben.zhongdian.system.entity.RentingHouse;
import xyz.ibenben.zhongdian.system.form.RentingHouseForm;

import java.util.List;

/**
 * 租赁房屋服务类
 * 提供了一些基本的服务，如获取图像等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface RentingHouseService extends IService<RentingHouse> {
    /**
     * 获取租赁信息带图像
     *
     * @param id 主键
     * @return 租赁信息
     */
    RentingHouse findOneWithImage(Long id);

    /**
     * 根据标题获取记录
     *
     * @param title 标题
     * @return 租赁信息
     */
    List<RentingHouse> findByTitle(String title);

    /**
     * 根据条件查询列表
     *
     * @param rentingHouseForm 条件
     * @return 列表
     */
    List<RentingHouse> findAll(RentingHouseForm rentingHouseForm);
}
