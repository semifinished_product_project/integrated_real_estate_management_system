package xyz.ibenben.zhongdian.system.service.sys;

import xyz.ibenben.zhongdian.system.entity.sys.SysUserRole;

/**
 * 系统用户角色服务类
 * 系统级用户所使用的，是权限系统中的一部分
 * 提供了一些基本的服务，如添加用户角色、根据用户主键删除记录等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface SysUserRoleService {
    /**
     * 添加用户角色
     *
     * @param userRole 参数
     */
    void addUserRole(SysUserRole userRole);

    /**
     * 根据用户主键删除记录
     *
     * @param key 参数
     * @return 返回值
     */
    int deleteByUserId(Object key);
}
