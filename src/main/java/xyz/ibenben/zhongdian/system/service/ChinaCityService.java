package xyz.ibenben.zhongdian.system.service;

import xyz.ibenben.zhongdian.system.entity.ChinaCity;

import java.util.List;

/**
 * 城市服务类
 * 此类提供对城市查询的服务，是三级联动里的第二级
 * 提供了一些基本的服务，如通过代码查列表、通过代码查名称等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface ChinaCityService extends IService<ChinaCity> {
    /**
     * 通过省代码查询列表
     *
     * @param code 参数
     * @return 返回值
     */
    List<ChinaCity> findListByProvinceCode(Long code);

    /**
     * 通过代码查询名称
     *
     * @param code 参数
     * @return 返回值
     */
    String findNameByCode(Long code);
}
