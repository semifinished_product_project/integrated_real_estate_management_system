package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.system.dao.SuggestInfoDao;
import xyz.ibenben.zhongdian.system.entity.SuggestInfo;
import xyz.ibenben.zhongdian.system.form.SuggestInfoForm;
import xyz.ibenben.zhongdian.system.mapper.SuggestInfoMapper;
import xyz.ibenben.zhongdian.system.service.SuggestInfoService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 意见及建议服务实现类
 * 提供了一些基本的服务，如根据条件查询列表等等方法。
 * 是用户在意见及建议时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class SuggestInfoServiceImpl extends AbstractServiceImpl<SuggestInfo> implements SuggestInfoService {
    @Resource
    private SuggestInfoDao suggestInfoDao;
    @Resource
    private SuggestInfoMapper suggestInfoMapper;

    @Override
    public List<SuggestInfo> findAll(SuggestInfoForm suggestInfoForm) {
        return suggestInfoMapper.findAll(suggestInfoForm);
    }

    @Override
    public SuggestInfo findByKey(Long id) {
        return suggestInfoMapper.findByKey(id);
    }
}
