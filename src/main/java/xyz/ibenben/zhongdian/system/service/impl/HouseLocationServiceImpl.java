package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.system.dao.HouseLocationDao;
import xyz.ibenben.zhongdian.system.entity.HouseLocation;
import xyz.ibenben.zhongdian.system.service.HouseLocationService;

import javax.annotation.Resource;

/**
 * 房屋位置服务实现类
 * 提供了一些基本的服务，如根据主键查找房屋位置
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class HouseLocationServiceImpl extends AbstractServiceImpl<HouseLocation> implements HouseLocationService {

    @Resource
    private HouseLocationDao houseLocationDao;

    @Override
    public HouseLocation selectByHouseId(Long houseId) {
        HouseLocation location = new HouseLocation();
        location.setHouseId(houseId);
        return houseLocationDao.selectOne(location);
    }
}
