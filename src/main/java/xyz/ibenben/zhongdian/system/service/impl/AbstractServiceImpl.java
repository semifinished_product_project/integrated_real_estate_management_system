package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.BaseEntity;
import xyz.ibenben.zhongdian.system.service.IService;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * 服务抽象类
 * 用于抽象出一些已知的服务，如根据主键获取唯一记录、保存记录等方法
 * 继承此类可以省去 大部分重复代码
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public abstract class AbstractServiceImpl<T extends BaseEntity> implements IService<T> {
    @Autowired
    private BaseDao<T> baseDao;

    /**
     * 根据主键获取唯一记录
     *
     * @param key 参数
     * @return 返回值
     */
    @SystemServiceLog(description = "根据主键获取唯一记录")
    @Override
    public T selectByKey(Object key) {
        return baseDao.selectByPrimaryKey(key);
    }

    /**
     * 删除记录
     *
     * @param key     参数
     * @param request 参数
     * @return 返回值
     */
    @Transactional(rollbackFor = Exception.class)
    @SystemServiceLog(description = "删除记录")
    @Override
    public int delete(Object key, HttpServletRequest request) {
        T entity = this.selectByKey(key);
        entity.setDelFlag(1);
        entity.setDelId((Long) request.getSession().getAttribute(Constants.SESSIONID));
        entity.setDelTime(new Date());
        return baseDao.updateByPrimaryKey(entity);
    }

    /**
     * 保存记录
     *
     * @param entity  参数
     * @param request 参数
     * @return 返回值
     */
    @Transactional(rollbackFor = Exception.class)
    @SystemServiceLog(description = "保存记录")
    @Override
    public int save(T entity, HttpServletRequest request) {
        entity.setCreateId((Long) request.getSession().getAttribute(Constants.SESSIONID));
        entity.setCreateTime(new Date());
        entity.setUpdateId((Long) request.getSession().getAttribute(Constants.SESSIONID));
        entity.setUpdateTime(new Date());
        return baseDao.insert(entity);
    }

    /**
     * 更新所有字段
     *
     * @param entity  参数
     * @param request 参数
     * @return 返回值
     */
    @Transactional(rollbackFor = Exception.class)
    @SystemServiceLog(description = "更新所有字段")
    @Override
    public int updateAll(T entity, HttpServletRequest request) {
        entity.setUpdateId((Long) request.getSession().getAttribute(Constants.SESSIONID));
        entity.setUpdateTime(new Date());
        return baseDao.updateByPrimaryKey(entity);
    }

    /**
     * 更新非空字段
     *
     * @param entity  参数
     * @param request 参数
     * @return 返回值
     */
    @Transactional(rollbackFor = Exception.class)
    @SystemServiceLog(description = "更新非空字段")
    @Override
    public int updateNotNull(T entity, HttpServletRequest request) {
        entity.setUpdateId((Long) request.getSession().getAttribute(Constants.SESSIONID));
        entity.setUpdateTime(new Date());
        return baseDao.updateByPrimaryKeySelective(entity);
    }

    /**
     * 获取所有记录
     *
     * @return 返回值
     */
    @SystemServiceLog(description = "获取所有记录")
    @Override
    public List<T> selectAll() {
        return baseDao.selectAll();
    }

    /**
     * 根据条件获取记录
     *
     * @param example 参数
     * @return 返回值
     */
    @SystemServiceLog(description = "根据条件获取记录")
    @Override
    public List<T> selectByExample(Example example) {
        return baseDao.selectByExample(example);
    }
}
