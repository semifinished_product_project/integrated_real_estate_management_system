package xyz.ibenben.zhongdian.system.service.sys.impl;

import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.dao.sys.SysResourcesDao;
import xyz.ibenben.zhongdian.system.entity.sys.SysResource;
import xyz.ibenben.zhongdian.system.entity.sys.SysResources;
import xyz.ibenben.zhongdian.system.form.sys.SysResourcesForm;
import xyz.ibenben.zhongdian.system.mapper.sys.SysResourcesMapper;
import xyz.ibenben.zhongdian.system.service.impl.AbstractServiceImpl;
import xyz.ibenben.zhongdian.system.service.sys.SysResourcesService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统资源服务实现类
 * 系统级资源管理所使用的，是权限系统中的一部分
 * 提供了一些基本的服务，如获取用户资源、查询资源列表等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service("sysResourcesService")
public class SysResourcesServiceImpl extends AbstractServiceImpl<SysResources> implements SysResourcesService {
    @Resource
    private SysResourcesMapper sysResourcesMapper;
    @Resource
    private SysResourcesDao sysResourcesDao;

    /**
     * loadUserResources资源记录
     *
     * @param userId 用户主键
     * @return 返回值
     */
    @Override
    //@Cacheable(cacheNames="resources",key="#map['userId'].toString()+#map['type']")
    @SystemServiceLog(description = "loadUserResources资源记录")
    public List<SysResources> loadUserResources(Long userId) {
        return sysResourcesMapper.loadUserResources(userId);
    }

    /**
     * queryResourcesListWithSelected资源记录
     *
     * @param rid 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "queryResourcesListWithSelected资源记录")
    public List<SysResource> queryResourcesListWithSelected(Long rid) {
        return sysResourcesMapper.queryResourcesListWithSelected(rid);
    }

    /**
     * selectByName资源记录
     *
     * @param name 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "selectByName资源记录")
    public SysResources selectByName(String name) {
        SysResources sysResources = null;
        Example example = new Example(SysResources.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("name", name);
        List<SysResources> resourcesList = sysResourcesDao.selectByExample(example);
        if (!resourcesList.isEmpty()) {
            sysResources = resourcesList.get(0);
        }
        return sysResources;
    }

    /**
     * findNameByResurl资源记录
     *
     * @param uri 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findNameByResurl资源记录")
    public SysResources findNameByResurl(String uri) {
        SysResources sysResources = null;
        Example example = new Example(SysResources.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("resUrl", uri);
        List<SysResources> resourcesList = sysResourcesDao.selectByExample(example);
        if (!resourcesList.isEmpty()) {
            sysResources = resourcesList.get(0);
        }
        return sysResources;
    }

    /**
     * findByType资源记录
     *
     * @param type 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findByType资源记录")
    public List<SysResources> findByType(int type) {
        Example example = new Example(SysResources.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("type", type);
        return sysResourcesDao.selectByExample(example);
    }

    @Override
    public List<SysResources> findAll(SysResourcesForm sysResourcesForm) {
        return sysResourcesMapper.findAll(sysResourcesForm);
    }
}
