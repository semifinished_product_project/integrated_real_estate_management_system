package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import xyz.ibenben.zhongdian.system.dao.StuffInfoDao;
import xyz.ibenben.zhongdian.system.entity.StuffInfo;
import xyz.ibenben.zhongdian.system.form.StuffInfoForm;
import xyz.ibenben.zhongdian.system.mapper.StuffInfoMapper;
import xyz.ibenben.zhongdian.system.service.StuffInfoService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 租赁物品信息服务实现类
 * 提供了一些基本的服务，如根据主键查找租赁物品信息等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class StuffInfoServiceImpl extends AbstractServiceImpl<StuffInfo> implements StuffInfoService {

    @Resource
    private StuffInfoDao stuffInfoDao;
    @Resource
    private StuffInfoMapper stuffInfoMapper;

    @Override
    public List<StuffInfo> selectByRentingHouseId(Long rentingId) {
        Example example = new Example(StuffInfo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("rentingId", rentingId);
        return stuffInfoDao.selectByExample(example);
    }

    @Override
    public List<StuffInfo> findAll(StuffInfoForm stuffInfoForm) {
        return stuffInfoMapper.findAll(stuffInfoForm);
    }

    @Override
    public StuffInfo findByKey(Long id) {
        return stuffInfoMapper.findByKey(id);
    }
}
