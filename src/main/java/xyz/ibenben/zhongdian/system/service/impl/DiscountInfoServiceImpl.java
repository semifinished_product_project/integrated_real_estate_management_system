package xyz.ibenben.zhongdian.system.service.impl;

import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.system.dao.DiscountInfoDao;
import xyz.ibenben.zhongdian.system.entity.DiscountInfo;
import xyz.ibenben.zhongdian.system.form.DiscountInfoForm;
import xyz.ibenben.zhongdian.system.mapper.DiscountInfoMapper;
import xyz.ibenben.zhongdian.system.service.DiscountInfoService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 折扣信息服务实现类
 * 提供了一些基本的服务，如根据主键查找房屋折扣信息等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class DiscountInfoServiceImpl extends AbstractServiceImpl<DiscountInfo> implements DiscountInfoService {

    @Resource
    private DiscountInfoDao discountInfoDao;
    @Resource
    private DiscountInfoMapper discountInfoMapper;

    @Override
    public List<DiscountInfo> findAll(DiscountInfoForm discountInfoForm) {
        return discountInfoMapper.findAll(discountInfoForm);
    }
}
