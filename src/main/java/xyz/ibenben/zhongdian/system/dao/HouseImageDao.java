package xyz.ibenben.zhongdian.system.dao;

import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.HouseImage;

/**
 * 房屋图像Dao类
 * 提供了一些基本的服务
 * 此类提供了所有系统里图像的处理功能
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface HouseImageDao extends BaseDao<HouseImage> {
    //房屋图像
}
