package xyz.ibenben.zhongdian.system.dao;

import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.Reminder;

/**
 * 系统提醒记录Dao类
 * 提供了一些基本的服务
 * 是用户在系统提醒时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface ReminderDao extends BaseDao<Reminder> {
    //系统提醒
}
