package xyz.ibenben.zhongdian.system.dao;

import org.apache.ibatis.annotations.Select;
import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.ChinaProvince;

/**
 * 省市Dao类
 * 此类提供对省市查询的服务，是三级联动里的第一级
 * 提供了一些基本的服务，如获取省份编码、根据编码查名称等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface ChinaProvinceDao extends BaseDao<ChinaProvince> {

    /**
     * 获取省份编码
     *
     * @param code 参数
     * @return 返回值
     */
    @Select("select province_name from china_province where province_code = #{code}")
    String findByCode(String code);

    /**
     * 根据编码查名称
     *
     * @param name 参数
     * @return 返回值
     */
    @Select("select province_code from china_province where province_name = #{name}")
    String findByName(String name);

}
