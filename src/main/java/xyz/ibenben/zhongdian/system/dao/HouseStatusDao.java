package xyz.ibenben.zhongdian.system.dao;

import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.HouseStatus;

/**
 * 房屋状态Dao类
 * 提供了一些基本的服务
 * 属于产权下的一个分支
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface HouseStatusDao extends BaseDao<HouseStatus> {
    //房屋状态
}
