package xyz.ibenben.zhongdian.system.dao.sys;

import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.sys.SysRole;

/**
 * 系统角色Dao类
 * 系统级角色管理所使用的，是权限系统中的一部分
 * 提供了一些基本的服务。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface SysRoleDao extends BaseDao<SysRole> {
    //系统角色
}
