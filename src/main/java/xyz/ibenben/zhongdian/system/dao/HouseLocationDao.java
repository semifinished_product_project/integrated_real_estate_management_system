package xyz.ibenben.zhongdian.system.dao;

import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.HouseLocation;

/**
 * 房屋位置Dao类
 * 提供了一些基本的服务
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface HouseLocationDao extends BaseDao<HouseLocation> {
    //房屋位置
}
