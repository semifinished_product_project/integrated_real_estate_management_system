package xyz.ibenben.zhongdian.system.dao;

import org.apache.ibatis.annotations.Select;
import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.ChinaCity;

/**
 * 城市Dao类
 * 此类提供对城市查询的服务，是三级联动里的第二级
 * 提供了一些基本的服务，如获取城市编码、根据编码查名称等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface ChinaCityDao extends BaseDao<ChinaCity> {

    /**
     * 获取城市编码
     *
     * @param code 参数
     * @return 返回值
     */
    @Select("select city_name from china_city where city_code = #{code}")
    String findByCode(String code);

    /**
     * 根据编码查名称
     *
     * @param name 参数
     * @param pId  参数
     * @return 返回值
     */
    @Select("select city_code from china_city where city_name = #{0} and province_code = #{1}")
    String findByName(String name, String pId);

}
