package xyz.ibenben.zhongdian.system.dao;

import xyz.ibenben.zhongdian.common.BaseDao;
import xyz.ibenben.zhongdian.system.entity.StuffInfo;

/**
 * 租赁物品信息Dao类
 * 提供了一些基本的服务
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface StuffInfoDao extends BaseDao<StuffInfo> {
    //租赁物品信息
}
