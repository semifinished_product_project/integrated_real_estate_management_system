var pieChat;

$(function () {
    getAjaxData();
});

function getAjaxData() {
    var pydata;
    var pieOption;
    $.ajax({
        url: "/houseInfo/getAllHouse",
        type: "post",
        dataType: "json",
        success: function (json) {
            if (json.success) {
                var mapReturn = json.attributes;
                var provincePieExist = mapReturn.provincePieExist;
                if (!provincePieExist || dataIsZero(mapReturn.provincePieChatY)) {
                    $("#echartPieProvince").hide();
                } else {
                    $("#echartPieProvince").show();
                    pydata = setYdataOfPie(mapReturn.provincePieChatX, mapReturn.provincePieChatY);
                    pieOption = setPieOption("各个省房屋信息占比", mapReturn.provincePieChatX, pydata, "数量");
                    pieChat = myChart('echartPieProvince', pieOption);
                }

                var cityPieExist = mapReturn.cityPieExist;
                if (!cityPieExist || dataIsZero(mapReturn.cityPieChatY)) {
                    $("#echartPieCity").hide();
                } else {
                    $("#echartPieCity").show();
                    pydata = setYdataOfPie(mapReturn.cityPieChatX, mapReturn.cityPieChatY);
                    pieOption = setPieOption("各个城市房屋信息占比", mapReturn.cityPieChatX, pydata, "数量");
                    pieChat = myChart('echartPieCity', pieOption);
                }

                var regionPieExist = mapReturn.regionPieExist;
                if (!regionPieExist || dataIsZero(mapReturn.regionPieChatY)) {
                    $("#echartPieRegion").hide();
                } else {
                    $("#echartPieRegion").show();
                    pydata = setYdataOfPie(mapReturn.regionPieChatX, mapReturn.regionPieChatY);
                    pieOption = setPieOption("各个区县房屋信息占比", mapReturn.regionPieChatX, pydata, "数量");
                    pieChat = myChart('echartPieRegion', pieOption);
                }

                var mapExist = mapReturn.mapExist;
                if(!mapExist){
                    $("#echartMap").hide();
                }else{
                    $("#echartMap").show();
                    pydata = mapReturn.mapData;
                    pieOption = setMapOption("全国主要城市房屋", "购买情况", "购买", "TOP5", pydata);
                    pieChat = myChart('echartMap', pieOption);
                }
            }
        }
    });
}

function dataIsZero(array) {
    if (array !== undefined) {
        var data = eval(array);
        var sum = 0;
        for (var i = 0; i < data.length; i++) {
            if (data[i] === 0) {
                sum++;
            }
        }
        if (sum === data.length) {
            return true;
        }
    }
    return false;
}

function setPieOption(title, xdata, ydata, name) {
    var option = {
        title: {text: title, x: 'center'},
        tooltip: {trigger: 'item', formatter: "{a} <br/>{b} : {c} ({d}%)"},
        legend: {
            type: 'scroll', orient: 'vertical', right: 10, top: 20, bottom: 20, data: eval(xdata),
            formatter: function (name) {
                return name.length < 15 ? name : name.substring(0, 15) + '...'
            }
        },
        series: [{
            name: name, type: 'pie', radius: '55%', center: ['40%', '50%'],
            data: ydata,
            itemStyle: {
                emphasis: {shadowBlur: 10, shadowOffsetX: 0, shadowColor: 'rgba(0, 0, 0, 0.5)'}
            },
            label: {
                normal: {
                    formatter(v) {
                        var text = v.data.name;
                        return text.length < 10 ? text : text.substring(0, 10) + '...'
                    }
                }
            }
        }]
    };
    return option;
}

function setYdataOfPie(pchatx, pchaty) {
    if (pchatx !== undefined && pchaty !== undefined) {
        var cols = eval(pchatx);
        var rows = eval(pchaty);
        var res = new Array();
        for (var i = 0; i < cols.length; i++) {
            res.push("{name : \"" + cols[i].replace(/\"/g, "\\\"") + "\",value : \"" + rows[i] + "\"}");
        }
        return eval("[" + res.join(",") + "]");
    }
}

function myChart(id, option) {
    var myChart = echarts.init(document.getElementById(id));
    myChart.setOption(option);
    return myChart;
}

$(window).resize(function () {
    resize();
});

function resize() {
    if (!$("#pie").is(":hidden")) {
        pieChat.resize();
    }
}

function setMapOption(title, subtext, seriesName, topName, data) {
    var option = {
        title: {
            text: title,
            subtext: subtext,
            left: 'center'
        },
        tooltip: {
            trigger: 'item',
            formatter: function (params, ticket, callback) {
                return params.seriesName + '</br>' + params.name + ":" + params.value[2];
            }
        },
        bmap: {
            center: [104.114129, 37.550339],
            zoom: 5,
            roam: true,
            mapStyle: {
                styleJson: [{
                    'featureType': 'water',
                    'elementType': 'all',
                    'stylers': {
                        'color': '#d1d1d1'
                    }
                }, {
                    'featureType': 'land',
                    'elementType': 'all',
                    'stylers': {
                        'color': '#f3f3f3'
                    }
                }, {
                    'featureType': 'railway',
                    'elementType': 'all',
                    'stylers': {
                        'visibility': 'off'
                    }
                }, {
                    'featureType': 'highway',
                    'elementType': 'all',
                    'stylers': {
                        'color': '#fdfdfd'
                    }
                }, {
                    'featureType': 'highway',
                    'elementType': 'labels',
                    'stylers': {
                        'visibility': 'off'
                    }
                }, {
                    'featureType': 'arterial',
                    'elementType': 'geometry',
                    'stylers': {
                        'color': '#fefefe'
                    }
                }, {
                    'featureType': 'arterial',
                    'elementType': 'geometry.fill',
                    'stylers': {
                        'color': '#fefefe'
                    }
                }, {
                    'featureType': 'poi',
                    'elementType': 'all',
                    'stylers': {
                        'visibility': 'off'
                    }
                }, {
                    'featureType': 'green',
                    'elementType': 'all',
                    'stylers': {
                        'visibility': 'off'
                    }
                }, {
                    'featureType': 'subway',
                    'elementType': 'all',
                    'stylers': {
                        'visibility': 'off'
                    }
                }, {
                    'featureType': 'manmade',
                    'elementType': 'all',
                    'stylers': {
                        'color': '#d1d1d1'
                    }
                }, {
                    'featureType': 'local',
                    'elementType': 'all',
                    'stylers': {
                        'color': '#d1d1d1'
                    }
                }, {
                    'featureType': 'arterial',
                    'elementType': 'labels',
                    'stylers': {
                        'visibility': 'off'
                    }
                }, {
                    'featureType': 'boundary',
                    'elementType': 'all',
                    'stylers': {
                        'color': '#fefefe'
                    }
                }, {
                    'featureType': 'building',
                    'elementType': 'all',
                    'stylers': {
                        'color': '#d1d1d1'
                    }
                }, {
                    'featureType': 'label',
                    'elementType': 'labels.text.fill',
                    'stylers': {
                        'color': '#999999'
                    }
                }]
            }
        },
        series: [
            {
                name: seriesName,
                type: 'scatter',
                coordinateSystem: 'bmap',
                data: convertData(data),
                symbolSize: function (val) {
                    return val[2];
                },
                label: {
                    normal: {
                        formatter: '{b}',
                        position: 'right',
                        show: false
                    },
                    emphasis: {
                        show: true
                    }
                },
                itemStyle: {
                    normal: {
                        color: 'purple'
                    }
                }
            },
            {
                name: topName,
                type: 'effectScatter',
                coordinateSystem: 'bmap',
                data: convertData(data.sort(function (a, b) {
                    return b.value - a.value;
                }).slice(0, 5)),
                symbolSize: function (val) {
                    return val[2];
                },
                showEffectOn: 'render',
                rippleEffect: {
                    brushType: 'stroke'
                },
                hoverAnimation: true,
                label: {
                    normal: {
                        formatter: '{b}',
                        position: 'right',
                        show: true
                    }
                },
                itemStyle: {
                    normal: {
                        color: 'purple',
                        shadowBlur: 10,
                        shadowColor: '#333'
                    }
                },
                zlevel: 1
            }
        ]
    };
    return option;
}

var convertData = function (data) {
    var res = [];
    for (var i = 0; i < data.length; i++) {
        var geoCoord = [];
        geoCoord.push(data[i].lng, data[i].lat);
        if (geoCoord) {
            res.push({
                name: data[i].name,
                value: geoCoord.concat(data[i].value)
            });
        }
    }
    return res;
};