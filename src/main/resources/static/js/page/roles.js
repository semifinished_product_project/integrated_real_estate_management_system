$(".checkall").click(function () {
    var check = $(this).prop("checked");
    $(".checkchild").prop("checked", check);
});
var table;
$(document).ready(function () {
    table = $('#datatable').DataTable({
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "searching": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "serverSide": true,//开启服务器模式，使用服务器端处理配置datatable
        "processing": true,//开启读取服务器数据时显示正在加载中……特别是大数据量的时候，开启此功能比较好

        "ajax": 'roles',
        "columns": [
            {
                "sClass": "text-center",
                "data": "id",
                "render": function (data, type, full, meta) {
                    return '<input type="checkbox"  class="checkchild"  value="' + data + '" />';
                },
                "bSortable": false
            },
            {"data": "id"},
            {"data": "roledesc"},
        ],
        columnDefs: [
            {"orderable": false, "targets": 1},
            {"orderable": false, "targets": 2},
        ],

    });
});

function search() {
    table.ajax.reload();
}

//弹出选择角色的框
var roleId;

function allotResources() {
    var rid = $(".checkchild:checked").val();
    if ($(".checkchild:checked").length < 1) {
        layer.msg('请选择一条数据');
        return;
    }
    if ($(".checkchild:checked").length > 1) {
        layer.msg('一次只能修改一条数据');
        return;
    }
    roleId = rid;
    var setting = {
        view: {
            showIcon: false,
            addDiyDom: addDiyDom
        },
        check: {
            enable: true,
            chkboxType: {"Y": "ps", "N": "s"}
        },
        data: {
            simpleData: {
                enable: true,
                idKey: "id",
                pIdKey: "parentid",
            }
        }
    };

    $.ajax({
        async: false,
        type: "POST",
        data: {rid: rid},
        url: "sysResources/resourcesWithSelected",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            $.fn.zTree.init($("#treeDemo"), setting, data);
            var zTree = $.fn.zTree.getZTreeObj("treeDemo");
            zTree.expandAll(true);
            $('#selectResources').modal();
        }
    });

}

function addDiyDom(treeId, treeNode) {
    if (treeNode.isLeaf === true) {
        $("#" + treeNode.tId).addClass("isLeaf");
    }
}

//保存权限的选择
function saveRoleResources() {
    var zTree = $.fn.zTree.getZTreeObj("treeDemo"), checkNode = zTree.getCheckedNodes(true);
    var ids = new Array();
    for (var i = 0; i < checkNode.length; i++) {
        ids.push(checkNode[i].id);
    }
    $.ajax({
        async: false,
        type: "POST",
        data: {roleid: roleId, resourcesid: ids.join(",")},
        url: "roles/saveRoleResources",
        success: function (data) {
            if (data == "success") {
                layer.msg('保存成功');
                $('#selectResources').modal('hide');
            } else {
                layer.msg('保存失败');
                $('#selectResources').modal('hide');
            }
        }
    });
}

//添加用户
function addRole() {
    var roleDesc = $("#roleDesc").val();
    if (roleDesc == "" || roleDesc == undefined || roleDesc == null) {
        return layer.msg('角色名称不能为空', function () {
            //关闭后的操作
        });
    }

    $.ajax({
        cache: true,
        type: "POST",
        url: 'roles/add',
        data: $('#roleForm').serialize(),// 你的formid
        async: false,
        success: function (data) {
            if (data == "success") {
                layer.msg('保存成功');
                table.ajax.reload();
                $('#addRole').modal('hide');
            } else {
                layer.msg('保存失败');
                $('#addRole').modal('hide');
            }
        }
    });
}


function delById() {
    var id = $(".checkchild:checked").val();
    if ($(".checkchild:checked").length < 1) {
        layer.msg('请选择一条数据');
        return;
    }
    if ($(".checkchild:checked").length > 1) {
        layer.msg('一次只能修改一条数据');
        return;
    }
    layer.confirm('您确定要删除该角色吗？', {
        btn: ['确认', '取消'] //按钮
    }, function () {
        $.ajax({
            cache: true,
            type: "POST",
            url: 'roles/delete',
            data: {id: id},
            async: false,
            success: function (data) {
                if (data == "success") {
                    layer.msg('删除成功');
                    table.ajax.reload();
                } else {
                    layer.msg('删除失败');
                }
            }
        });
    });


}